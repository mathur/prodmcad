# Inspired by design on: https://www.thingiverse.com/thing:3859829
import cadquery as cq
import Part
# configuration
# Enter desired diamanter of plates
# d1=54.7
d1 = 40.0

# enter desired diameter of spacers
sp = 4.0

# enter top bay height
tb = 50.0
#  shelf spacing
ss = 10.0
# enter shelf thickness
st = 2.0

ra1 = d1/2.0
ra2 = ((ra1)* -1.0)
sp1 = sp/2.0
th = (ss*2.0)+(st*4.0) + tb

# Base plate
base = cq.Workplane("XY").circle(ra1).extrude(st)

# Layer 2 plate
base = base.faces(">Z").workplane(offset=ss).circle(ra1).extrude(st)
# layer_3
base = base.faces(">Z").workplane(offset=ss).circle(ra1).extrude(st)

# Top plate
top = cq.Workplane("XY").workplane(offset=th - st).circle(ra1).extrude(st)

base = base.union(top)

# Holders
holders_a = cq.Workplane("XY").transformed(offset=((ra1 - (sp / 2)), 0, 0)).circle(sp1).extrude(th, combine=False)
holders_b = cq.Workplane("XY").transformed(offset=((ra2 + (sp / 2)), 0, 0)).circle(sp1).extrude(th, combine=False)
holders_c = cq.Workplane("XY").transformed(offset=(0, (ra1 - (sp / 2.0)), 0)).circle(sp1).extrude(th, combine=False)
holders_d = cq.Workplane("XY").transformed(offset=(0, (ra2 + (sp / 2)), 0)).circle(sp1).extrude(th, combine=False)

holders = holders_a.union(holders_b.union(holders_c.union(holders_d)))

# Combine the holders with the base
result = base.union(holders)

# Export to FreeCAD
#Part.show(result.toFreecad())
# Export STL
f = open('Exports\\ElectronicsBay.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()