# Inspired by: thingiverse.com/thing:3848378

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer
print("SpokeWheel")

# Parameters
wheel_radius=50.0  # size of the wheel, default: 50
axle_radius=10.5 # size of the hole in the middle of the wheel, default: 10.5
wheel_width=10.0 # Default: 10

# ----------------------------------------
# actual code - no need to change anything

wheel = cq.Workplane("XY").circle(wheel_radius+1).extrude(wheel_width)
wheel_cut = cq.Workplane



result = frame.union(fins.intersect(common_with))
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\SpokeWheel.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.01)
f.close()