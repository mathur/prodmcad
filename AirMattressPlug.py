# Inspired by the model at: https://www.thingiverse.com/thing:3818734

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("AirMattressPlug")
# Diameter at bottom of plug
BottomDiameter = 22

# Diameter at top of plug
TopDiameter = 28

# Length of plug
PlugLength = 50

# Handle width
HandleWidth=50

result = cq.Workplane("XY").circle(radius=BottomDiameter/2.0).workplane(offset=PlugLength).circle(TopDiameter/2.0).loft()
result = result.faces(">Z").box(10,HandleWidth,10)

# Export to FreeCAD
#Part.show(result.toFreecad())
# Export STL
f = open('Exports\\AirMattressPlug.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()