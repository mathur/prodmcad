# Inspired by the model at: thingiverse.com/thing:3811422

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("EyepieceHolder")
inner_diameter=25.4*(1+1/4.0)+0  # +0 adjustment factor to account for
outer_diameter=25.4*(1+3/8.0)+0

height=30 # in mm
screw_hole_from_top=6 #  in mm
screw_hole_border=6 #  in mm
screw_hole_depth=5 # how deep is the screw
screw_size=2.5 # M3-0.5 # Screw size-pitch To allow for tapping. 
#  If you want to use a nut instead of tapping, set screw_nut equal to 6, screw_nut_depth, and screw_nut_head. Set the screw size to full diameter, and also make sure screw_hole_border, and screw_hole_depth are large enout
screw_nut = 360
screw_nut_depth=1 
screw_nut_head=4 # tip to tip, not flat to flat. So generally the wrench size *sqrt(2)

base = cq.Workplane("XY").circle(outer_diameter/2.0).extrude(height)
base_workplane = base.faces(">Z").workplane()
hole_base = cq.Workplane("XY")
for i in range(0, 3):
    temp = base_workplane.transformed(rotate=(0,0,i*120))\
        .transformed(offset=((-inner_diameter/2.0)-screw_hole_depth, -screw_hole_border/2.0, -screw_hole_from_top))\
        .box(outer_diameter/2.0 + screw_hole_depth, screw_hole_border, screw_hole_border, combine=False, centered=(False, False, False))

    hole_base = hole_base.union(temp)

result = base.union(hole_base)
# Hole with inner diameter
result.faces(">Z").hole(diameter=inner_diameter, depth=height)
# Nut cut out (parameterized cut shape)
base_workplane = base.faces(">Z").workplane()
cuts = cq.Workplane("XY")
for i in range(0, 3):
    # TODO: Fix rotations for cut
    temp = base_workplane.transformed(offset=(0, 0, -screw_hole_from_top/2.0)) \
            .transformed(rotate=(0, -90, i * 120)) \
    # We assume it is either hex or circular
    if screw_nut != 6:
        temp = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
    else:
        temp_1 = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
        temp_2 = temp.polygon(nSides=screw_nut, diameter=screw_nut_head).extrude(screw_nut_depth, combine=False)
        temp = temp_1.union(temp_2)
    cuts = cuts.union(temp)
result = result.cut(cuts)
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\EyepieceHolder.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()
