# Inspired by https://www.thingiverse.com/thing:3814727

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer
import math

print("BoltCap")
# Distance across flats of nut in mm.
nut_across_flats_mm = 19.4 # min:1

# Height of the nut in mm.
nut_height_mm = 13 # min:1

# Length of the bolt shaft in mm (excludes bolt head).
bolt_length_mm = 33 # min:1

# Diameter of the bolt shaft in mm
bolt_diameter_mm = 13.2 # min:1

# Diameter of the air hole in mm. Use 0 if no hole is required.
air_hole_diameter_mm = 3 # min:0

# The wall thickness in mm.
wall_thickness_mm = 1.2 # min:1

# Additional clearance in mm for the bolt shaft.
bolt_clearance_mm= 0.5 # min:0


# [Hidden]


# Given the distance across flats of a hex, find the radius of a circle that scribes around the flats.
def hex_flats_to_radius(flats):
    return (flats/2.0)/math.cos(math.radians(180.0/6.0))


bolt_rad = bolt_diameter_mm/2.0
nut_internal_rad=hex_flats_to_radius(nut_across_flats_mm)
nut_external_rad=hex_flats_to_radius(nut_across_flats_mm + wall_thickness_mm)
air_hole_rad = air_hole_diameter_mm/2

# Outer shell
cyl = cq.Workplane("XY").circle(nut_external_rad).extrude(bolt_length_mm - nut_external_rad/3.0)
outer_shell = cyl.faces(">Z").sphere(nut_external_rad)

# Inner cuts

# Bolt cut - with slope on top
cuts = cq.Workplane("XY").circle((bolt_diameter_mm + bolt_clearance_mm)/2.0).extrude(bolt_length_mm + bolt_clearance_mm)
# Try to make a cone
cuts = cuts.faces(">Z").circle(bolt_rad).workplane(offset=bolt_rad).circle(0.00001).loft()
# Nut cut with slope on top

cuts_hex = cq.Workplane("XY").polygon(nSides=6, diameter=2.0*nut_internal_rad).extrude(nut_height_mm + bolt_clearance_mm)
cuts_hex = cuts_hex.faces(">Z").polygon(nSides=6, diameter=2.0*nut_internal_rad).workplane(offset=nut_height_mm/2.0 + bolt_clearance_mm).polygon(nSides=6, diameter=bolt_diameter_mm).loft()
# Union the cuts
cuts = cuts.union(cuts_hex)
result = outer_shell.cut(cuts)
# Air hole
result = result.faces(">Z").hole(diameter=air_hole_diameter_mm, depth=bolt_length_mm + bolt_clearance_mm + nut_external_rad + (wall_thickness_mm*2))
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\BoltCap.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()