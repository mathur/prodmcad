'''
Parametric Turners Cube
Link to similar Thingiverse file: https://www.thingiverse.com/thing:1072045
'''


import cadquery as cq
import random
import json

# Number of iterations for the model
num_iter = 50

print("Circular Hole Generator")

L = 100.0   # default 100

D1 = 90.0   # default 90

D2 = 60.0  # default 60;

D3 = 30.0  # default 30;


T = L/8.0;
X1 = L/2.0-T;
X2 = L/2.0-2.0*T;
X3 = L/2.0-3.0*T;

# Make the cube
cube = cq.Workplane("XY").box(L,L,L)


# Add smallestholes

cube = cube.faces(">X").workplane().hole(D3, depth=X1)
cube = cube.faces("<X").workplane().hole(D3, depth=X1)
cube = cube.faces(">Y").workplane().hole(D3, depth=X1)
cube = cube.faces("<Y").workplane().hole(D3, depth=X1)
cube = cube.faces(">Z").workplane().hole(D3, depth=X1)
cube = cube.faces("<Z").workplane().hole(D3, depth=X1)

cube = cube.faces(">X").workplane().hole(D2, depth=X2)
cube = cube.faces("<X").workplane().hole(D2, depth=X2)
cube = cube.faces(">Y").workplane().hole(D2, depth=X2)
cube = cube.faces("<Y").workplane().hole(D2, depth=X2)
cube = cube.faces(">Z").workplane().hole(D2, depth=X2)
cube = cube.faces("<Z").workplane().hole(D2, depth=X2)

cube = cube.faces(">X").workplane().hole(D1, depth=X3)
cube = cube.faces("<X").workplane().hole(D1, depth=X3)
cube = cube.faces(">Y").workplane().hole(D1, depth=X3)
cube = cube.faces("<Y").workplane().hole(D1, depth=X3)
cube = cube.faces(">Z").workplane().hole(D1, depth=X3)
result = cube.faces("<Z").workplane().hole(D1, depth=X3)

# Export STL
f = open('Exports\\TurnersCube.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()


