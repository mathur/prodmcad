# Inspired by the design at: https:#www.thingiverse.com/thing:3841621
import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("LockShaft")
# [Dimensions of triangular drive:] 
# (across points measurment)
Key_AP = 9.2
# (length)
Key_l = 8.5
# (tip radius)
Key_tr = 0.5

# [Flange dimensions] 
# (diameter)
Flange_d = 15.8
# (length)
Flange_l = 1.9

# [Shaft dimensions] 
# (diameter)
Shaft_d = 9.8
# (length)
Shaft_l = 5.7

# [Locking pawl drive boss dimensions] 
# (width)
Drive_w = 7.1
# (breadth)
Drive_b = 6.1
# (length)
Drive_l = 9.1
# (angle offset)
Drive_a = 0

# Not to be changed (advice from OpenSCAD)
Key_r = (0.57735 * Key_AP) - Key_tr
Total_l = Key_l + Flange_l + Shaft_l + Drive_l

base = cq.Workplane("XY").polygon(nSides=3, diameter=(4.0 / 1.73205) * (Key_r + Key_tr)).extrude(Key_l)
base = base.edges("|Z").fillet(Key_tr)

flange = base.faces(">Z").circle(Flange_d/2.0).extrude(Flange_l)
shaft = flange.faces(">Z").workplane(offset=-0.01).circle(Shaft_d/2.0).extrude(Shaft_l + 0.01)

drive = shaft.faces(">Z").workplane().transformed(offset=(0,0,(Drive_l/2.0)-0.01), rotate=(0,0,Drive_a))\
    .box(Drive_b, Drive_w, Drive_l + 0.02)

cut_a = cq.Workplane("XY").workplane(offset=0.5).circle(2.8/2.0).extrude(0.6*Total_l)
cut = cut_a.faces(">Z").workplane(offset=0.49).circle(3.25/2.0).extrude(0.4*Total_l)

result = drive.cut(cut)
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\LockShaft.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.01)
f.close()