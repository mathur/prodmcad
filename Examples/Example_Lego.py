# This script can create any regular rectangular Lego(TM) Brick
import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

#####
# Inputs
######
lbumps = 2       # number of bumps long
wbumps = 2        # number of bumps wide
thin = True  # True for thin, False for thick

#
# Lego Brick Constants-- these make a lego brick a lego :)
#
pitch = 8.0
clearance = 0.1
bumpDiam = 4.8
bumpHeight = 1.8
if thin:
    height = 3.2
else:
    height = 9.6

t = (pitch - (2 * clearance) - bumpDiam) / 2.0
postDiam = pitch - t  # works out to 6.5
total_length = lbumps*pitch - 2.0*clearance
total_width = wbumps*pitch - 2.0*clearance

#Create tracker object
tracker = shape_analyzer.Tracker()

# make the base
s = cq.Workplane("XY").box(total_length, total_width, height)
tracker.add_change(shape_analyzer.get_line_num() - 1, s)
# shell inwards not outwards
s = s.faces("<Z").shell(-1.0 * t)
tracker.add_change(shape_analyzer.get_line_num() - 1, s)
# make the bumps on the top
extr = s.faces(">Z").workplane(). \
    rarray(pitch, pitch, lbumps, wbumps, True).circle(bumpDiam / 2.0).extrude(bumpHeight, combine=False, keep_stack=True)
tracker.add_change(shape_analyzer.get_line_num() - 1, extr)
s = extr.combine_stack(base=s)
tracker.add_change(shape_analyzer.get_line_num() - 1, s)

# add posts on the bottom. posts are different diameter depending on geometry
# solid studs for 1 bump, tubes for multiple, none for 1x1
tmp = s.faces("<Z").workplane(invert=True)
#tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)

if lbumps > 1 and wbumps > 1:
    tmp = tmp.rarray(pitch, pitch, lbumps - 1, wbumps - 1, center=True). \
        circle(postDiam / 2.0).circle(bumpDiam / 2.0).extrude(height - t)
    tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)

elif lbumps > 1:
    tmp = tmp.rarray(pitch, pitch, lbumps - 1, 1, center=True). \
        circle(t).extrude(height - t)
    tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)

elif wbumps > 1:
    tmp = tmp.rarray(pitch, pitch, 1, wbumps - 1, center=True). \
        circle(t).extrude(height - t)
    tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)

else:
    tmp = s
    tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)

# Render the solid
Part.show(tmp.toFreecad())

print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, tmp)
listener.start_keyboard()
