"""
This example imports a FreeCAD object and performs some operations on it.
Note: Ideally we'd like to find the top level object automatically, but doing this wasn't successful.
Therefore, we need the file name and object name.
"""


import os
import cadquery
import FreeCAD
import Part
from prodm import gui_observer
from prodm import shape_analyzer



file_path = "C:\Users\mpirron\PyCharmProjects\prodmcad\T02_Whiffle_Ball.fcstd"


cad_obj_name = "Cut005"
# Open the file as a document in FreCAD
doc = FreeCAD.openDocument(file_path)
# Set this as the active document in FreeCAD
FreeCAD.ActiveDocument = doc

# Set up the prodm tracker
tracker = shape_analyzer.Tracker()

# Get the object from FreeCAD document
obj_cq = None
"""
for obj in FreeCAD.ActiveDocument.Objects:
    if hasattr(obj,"Shape"):
        print("Using", obj, " as the shape.")
        obj_cq = cadquery.CQ(cadquery.Solid(obj.Shape))
        break
"""

obj_cad = FreeCAD.ActiveDocument.getObject(cad_obj_name)
obj_cq = cadquery.CQ(cadquery.Solid(obj_cad.Shape))
tracker.add_change(shape_analyzer.get_line_num() - 1, obj_cq)

# Make some CQ changes to the object
# newThing = obj_cq.edges(">Z or <Z").chamfer(.5)
newDoc = FreeCAD.newDocument("mod")
"""
# Create a new FreeCAD document
newDoc = FreeCAD.newDocument("mod")

# Add a FreeCAD object to the tree and then store a CQ object in it
nextShape = newDoc.addObject("Part::Feature", "nextShape")
nextShape.Shape = obj_cq.val().wrapped

# Re-render the doc to see what the new solid looks like
newDoc.recompute()
"""
Part.show(obj_cq.toFreecad())
print("Starting mouse event listener..")
listener = gui_observer.ViewListener(tracker, obj_cq)
listener.start_keyboard()

