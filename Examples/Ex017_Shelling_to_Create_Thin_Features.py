import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

# Create a hollow box that's open on both ends with a thin wall.
# 1.  Establishes a workplane that an object can be built on.
# 1a. Uses the named plane orientation "front" to define the workplane, meaning
#     that the positive Z direction is "up", and the negative Z direction
#     is "down".
# 2.  Creates a plain box to base future geometry on with the box() function.
# 3.  Selects faces with normal in +z direction.
# 4.  Create a shell by cutting out the top-most Z face.
tracker = shape_analyzer.Tracker()
result = cq.Workplane("front").box(2, 2, 2)#.faces("+Z").shell(0.05)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script
#show_object(result)
Part.show(result.toFreecad())
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
