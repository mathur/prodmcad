import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer
tracker = shape_analyzer.Tracker()
# Set up the length, width, and thickness
(L, w, t) = (20.0, 6.0, 3.0)
s = cq.Workplane("XY")

# Draw half the profile of the bottle and extrude it
result = s.center(-L / 2.0, 0).vLine(w / 2.0) \
     .threePointArc((L / 2.0, w / 2.0 + t), (L, w / 2.0)).vLine(-w / 2.0) \
     .mirrorX().extrude(30.0, True)

# Make the neck
result.faces(">Z").workplane().circle(3.0).extrude(2.0, True)

# Make a shell
#result = result.faces(">Z").shell(0.3)

tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script
#show_object(result)
Part.show(result.toFreecad())
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
