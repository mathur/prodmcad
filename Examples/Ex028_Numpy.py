import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer
tracker = shape_analyzer.Tracker()

import numpy as np

# Square side and offset in x and y.
side = 10
offset = 5

# Define the locations that the polyline will be drawn to/thru.
# The polyline is defined as numpy.array so that operations like translation
# of all points are simplified.
pts = np.array([
    (0, 0),
    (side, 0),
    (side, side),
    (0, side),
    (0, 0)
]) + [offset, offset]

result = cq.Workplane('XY') \
    .polyline(pts).close().extrude(2) \
    .faces('+Z').workplane().circle(side / 2).extrude(1)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script
#show_object(result)
Part.show(result.toFreecad())
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
