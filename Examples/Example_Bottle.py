import Part
import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Set up the length, width, and thicknesscy;
base = cadquery.Workplane("XY")

# Draw half the profile of the bottle and extrude it
cyl1 = base.box(15, 15, 30)
tracker.add_change(shape_analyzer.get_line_num() - 1, cyl1, "cyl1", ["base"])
cyl2 = cyl1.faces(">Z").workplane().box(10, 10, 20)
tracker.add_change(shape_analyzer.get_line_num() - 1, cyl2, "cyl2", ["cyl1"])

result = cyl1.union(cyl2)
tracker.add_change(shape_analyzer.get_line_num() - 1, result, "result", ["cyl1", "cyl2"])
result = result.faces(">Z").shell(-1.0)
tracker.add_change(shape_analyzer.get_line_num() - 1, result, "result", ["result"])
# result = result.edges("not >Z").edges("#Z").edges(">Z").fillet(1)
# result = result.edges("(not >Z) and ((not <Z) and #Z)").fillet(1)

# Displays the result of this script
Part.show(result.toFreecad())
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()