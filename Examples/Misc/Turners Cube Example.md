# Turners Cube Example

In this example, we compare the size of various parts of the turners
cube to the resulting face selector. All dimensions, unless otherwise
stated, are in mm.

## Dimensions
Dimension of the complete Turners Cube: 100x100x100 
Dimensions of the cutouts:

Radius of cutout forming the cube containing the 
* outer face: 93.75
* middle face: 62.5
* inner face 31.25

Each cutout has a depth of 25.

Dimensions of innermost cube (which has no cutouts): 25x25x25

## Generated Bounding Boxes 

### Outer faces
('The naive bounding box is=', [[-100.00000001, -100.00000001, -100.00000001], [100.00000001, 100.00000001, 100.00000001]])
Checking for side effects now...
('Side effect of applying this bounding box=', [806077440, 1186453696, 362694914, 1959789379, 184476598, 295374177, 1033196872, -511792439, -1385859253, -1069702517, 2080249026, -1350178739, -709403889, 1588724965, 1146935124, -1058110313, -273955637, -1500840988, 102476123, 1859441308, -372182171, 1951451169, -1567704605, -1889539932, 383228261, -80317191, 1020615463, -1102926360, 287725305, 1417600727, 525421810, -1780964107, -1512913482, -6977991, 1551031676, -2120389377])
('Bounding box selection took ', 0.16000008583068848)

### Middle faces
('The naive bounding box is=', [[-75.00000000999974, -75.00000001000171, -75.00000001000048], [75.00000001000085, 75.00000001000039, 75.0000000100011]])
Checking for side effects now...
('Side effect of applying this bounding box=', [806077440, 1186453696, 362694914, 184476598, 1951451169, 1033196872, -511792439, -1567704605, -1058110313, -1500840988, -80317191, 1859441308, 1588724965, 102476123, -1889539932, 383228261, 2080249026, 287725305, -1102926360, -1512913482, -372182171, -6977991, 1551031676, -2120389377])
('Bounding box selection took ', 0.1380000114440918)

### Inner facces
('The naive bounding box is=', [[-50.00000000999986, -50.52296401000001, -50.38798279016998], [50.52296401000003, 50.00000000999996, 50.38798279016994]])
Checking for side effects now...
('Side effect of applying this bounding box=', [1186453696, 2080249026, 184476598, 1033196872, -511792439, -1058110313, -6977991, 1859441308, 1951451169, -1567704605, -1500840988, -1102926360])
('Bounding box selection took ', 0.09599995613098145)

### Faces of uncut cube
('The naive bounding box is=', [[-25.00000000999988, -25.000000010000388, -25.00000000999972], [25.000000010000356, 25.00000000999988, 25.000000010000548]])
Checking for side effects now...
('Side effect of applying this bounding box=', [])
('Bounding box selection took ', 0.054000139236450195)


## Complete Logs / Outputs

Turners Cube Bounding Box Outer faces

('Creating tracked object entry for line', 40)
('This line has', 1, ' objects in stack.')
('Edges:', 234)
('Faces:', 42)
('Vertices:', 140)
----------
Starting mouse event listener..
Press Enter to continue...
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Success! Face found in', [40, 40, 40, 40, 40, 40])
('The required line number is', 40)
Synthesis: building a bounding box...
('The naive bounding box is=', [[-100.00000001, -100.00000001, -100.00000001], [100.00000001, 100.00000001, 100.00000001]])
Checking for side effects now...
('Side effect of applying this bounding box=', [806077440, 1186453696, 362694914, 1959789379, 184476598, 295374177, 1033196872, -511792439, -1385859253, -1069702517, 2080249026, -1350178739, -709403889, 1588724965, 1146935124, -1058110313, -273955637, -1500840988, 102476123, 1859441308, -372182171, 1951451169, -1567704605, -1889539932, 383228261, -80317191, 1020615463, -1102926360, 287725305, 1417600727, 525421810, -1780964107, -1512913482, -6977991, 1551031676, -2120389377])
('Bounding box selection took ', 0.16000008583068848)
Synthesis: using local variables to build bounding box
('Synthesized var bounding box is', [['-_INF_', '-_INF_', '-_INF_'], ['_INF_', '_INF_', '_INF_']], ' score (0 is best, lower is better)=', 48)
('Bounding box with var selection took ', 0.1659998893737793)
Synthesis: using aggregated lists and a decision tree
('Formula generated is', ' or  and %PLANE and >X or  and %PLANE and  not >X and >Y or  and %PLANE and  not >X and  not >Y and >Z or  and %PLANE and  not >X and  not >Y and  not >Z and <Z or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and <X or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and <Y')
('Tree built and it took', 0.005000114440917969, 'seconds')
Synthesis: using aggregated lists and decision tree (chaining)
('Formula generated is', ' or  . %PLANE . >X or  . %PLANE .  not >X . >Y or  . %PLANE .  not >X .  not >Y . >Z or  . %PLANE .  not >X .  not >Y .  not >Z . <Z or  . %PLANE .  not >X .  not >Y .  not >Z .  not <Z . <X or  . %PLANE .  not >X .  not >Y .  not >Z .  not <Z .  not <X . <Y')
('Tree built and it took', 14.217000007629395, 'seconds')
Synthesis: using aggregated lists and decision tree (adaptive)
('Formula generated is', ' or  and %PLANE and >X or  and %PLANE and  not >X and >Y or  and %PLANE and  not >X and  not >Y and >Z or  and %PLANE and  not >X and  not >Y and  not >Z and <Z or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and <X or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and <Y')
('Tree built and it took', 14.19700002670288, 'seconds')

Turners Cube Bounding Box middle faces

('Creating tracked object entry for line', 40)
('This line has', 1, ' objects in stack.')
('Edges:', 234)
('Faces:', 42)
('Vertices:', 140)
----------
Starting mouse event listener..
Press Enter to continue...
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Success! Face found in', [40, 40, 40, 40, 40, 40])
('The required line number is', 40)
Synthesis: building a bounding box...
('The naive bounding box is=', [[-75.00000000999974, -75.00000001000171, -75.00000001000048], [75.00000001000085, 75.00000001000039, 75.0000000100011]])
Checking for side effects now...
('Side effect of applying this bounding box=', [806077440, 1186453696, 362694914, 184476598, 1951451169, 1033196872, -511792439, -1567704605, -1058110313, -1500840988, -80317191, 1859441308, 1588724965, 102476123, -1889539932, 383228261, 2080249026, 287725305, -1102926360, -1512913482, -372182171, -6977991, 1551031676, -2120389377])
('Bounding box selection took ', 0.1380000114440918)
Synthesis: using local variables to build bounding box
('Synthesized var bounding box is', [['-_INF_', '-_INF_', '-_INF_'], ['_INF_', '_INF_', '_INF_']], ' score (0 is best, lower is better)=', 48)
('Bounding box with var selection took ', 0.14100003242492676)
Synthesis: using aggregated lists and a decision tree
Failing, no appropriate information gain.
Tree could not be built...
Synthesis: using aggregated lists and decision tree (chaining)
('Formula generated is', ' or  . %PLANE .  not >X . >X or  . %PLANE .  not >X .  not >X .  not +X .  not >X .  not >Y . >Y or  . %PLANE .  not >X .  not >X .  not +X .  not >X .  not >Y .  not >Y .  not +Y .  not >Z . >Z or  . %PLANE .  not >X .  not >X .  not +X .  not >X .  not >Y .  not >Y .  not +Y .  not >Z .  not >Z .  not +Z .  not <Z . <Z or  . %PLANE .  not >X .  not >X .  not +X .  not >X .  not >Y .  not >Y .  not +Y .  not >Z .  not >Z .  not +Z .  not <Z .  not <Z .  not >X . <Y or  . %PLANE .  not >X .  not >X .  not +X .  not >X .  not >Y .  not >Y .  not +Y .  not >Z .  not >Z .  not +Z .  not <Z .  not <Z .  not >X .  not <Y . >Y .  not >X . >X')
('Tree built and it took', 26.353999853134155, 'seconds')
Synthesis: using aggregated lists and decision tree (adaptive)
('Formula generated is', ' or  and %PLANE and  not >X . >X or  and %PLANE and  not >X .  not >X and  not +X and  not >Y . >Y or  and %PLANE and  not >X .  not >X and  not +X and  not >Y .  not >Y and  not +Y . >Y and  not >Z . >Z or  and %PLANE and  not >X .  not >X and  not +X and  not >Y .  not >Y and  not +Y .  not >Y and  not #Y . <X or  and %PLANE and  not >X .  not >X and  not +X and  not >Y .  not >Y and  not +Y . >Y and  not >Z .  not >Z and  not >Z and  not <Z . <Z or  and %PLANE and  not >X .  not >X and  not +X and  not >Y .  not >Y and  not +Y . >Y and  not >Z .  not >Z and  not >Z and  not <Z .  not <Z and  not >X and  not <X .  not >X')
('Tree built and it took', 22.450999975204468, 'seconds')

Turners Cube Bounding Box inner faces

('Creating tracked object entry for line', 40)
('This line has', 1, ' objects in stack.')
('Edges:', 234)
('Faces:', 42)
('Vertices:', 140)
----------
Starting mouse event listener..
Press Enter to continue...
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Success! Face found in', [40, 40, 40, 40, 40, 40])
('The required line number is', 40)
Synthesis: building a bounding box...
('The naive bounding box is=', [[-50.00000000999986, -50.52296401000001, -50.38798279016998], [50.52296401000003, 50.00000000999996, 50.38798279016994]])
Checking for side effects now...
('Side effect of applying this bounding box=', [1186453696, 2080249026, 184476598, 1033196872, -511792439, -1058110313, -6977991, 1859441308, 1951451169, -1567704605, -1500840988, -1102926360])
('Bounding box selection took ', 0.09599995613098145)
Synthesis: using local variables to build bounding box
('Synthesized var bounding box is', [['-_INF_', '-_INF_', '-_INF_'], ['_INF_', '_INF_', '_INF_']], ' score (0 is best, lower is better)=', 48)
('Bounding box with var selection took ', 0.10599994659423828)
Synthesis: using aggregated lists and a decision tree
Failing, no appropriate information gain.
Tree could not be built...
Synthesis: using aggregated lists and decision tree (chaining)
('Formula generated is', ' or  . %PLANE .  not >X .  not >X . >X or  . %PLANE .  not >X .  not >X .  not >X .  not >X . >X or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X .  not >X .  not >Y .  not <Y or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X . >X . +Y .  not >Y .  not >Y . >Y')
('Tree built and it took', 18.292999982833862, 'seconds')
Synthesis: using aggregated lists and decision tree (adaptive)
('Formula generated is', ' or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and >X or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not >Y . >Y or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not >Y .  not >Y and  not >X . >X or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not >Y .  not >Y and  not >X .  not >X and  not >X and |Z or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not >Y .  not >Y and  not >X .  not >X and  not >X and  not |Z .  not >Y and  not >X')
('Tree built and it took', 28.163000106811523, 'seconds')


Turners Cube Bounding Box innermost cube faces

('Creating tracked object entry for line', 40)
('This line has', 1, ' objects in stack.')
('Edges:', 234)
('Faces:', 42)
('Vertices:', 140)
----------
Starting mouse event listener..
Press Enter to continue...
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Looking for object in line', 40)
('Found in', 40)
('Success! Face found in', [40, 40, 40, 40, 40, 40])
('The required line number is', 40)
Synthesis: building a bounding box...
('The naive bounding box is=', [[-25.00000000999988, -25.000000010000388, -25.00000000999972], [25.000000010000356, 25.00000000999988, 25.000000010000548]])
Checking for side effects now...
('Side effect of applying this bounding box=', [])
('Bounding box selection took ', 0.054000139236450195)
Synthesis: using local variables to build bounding box
('Synthesized var bounding box is', [['-_INF_', '-_INF_', '-_INF_'], ['_INF_', '_INF_', '_INF_']], ' score (0 is best, lower is better)=', 48)
('Bounding box with var selection took ', 0.07299995422363281)
Synthesis: using aggregated lists and a decision tree
Failing, no appropriate information gain.
Tree could not be built...
Synthesis: using aggregated lists and decision tree (chaining)
('Formula generated is', ' or  . %PLANE .  not >X .  not >X .  not >X . >X or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X .  not >X . >Y . >X or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X . >X .  not >Y .  not >Y .  not >Y . >Y or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X . >X .  not >Y .  not >Y .  not >Y .  not >Y .  not >Z .  not >Z . >Z or  . %PLANE .  not >X .  not >X .  not >X .  not >X .  not >X . >X .  not >Y .  not >Y .  not >Y .  not >Y .  not >Z .  not >Z .  not >Z .  not <Z .  not <Z .  not <Y')
('Tree built and it took', 25.417999982833862, 'seconds')
Synthesis: using aggregated lists and decision tree (adaptive)
('Formula generated is', ' or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and +X or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not +X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and >X and  not >Y or  and %PLANE and  not >X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X and  not +X and  not >Y and  not >Z and  not <Z and  not <X and  not <Y .  not >X and  not >X . >Y')
('Tree built and it took', 31.32200002670288, 'seconds')


