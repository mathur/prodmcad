# Requirements
[FreeCAD](https://www.freecadweb.org/)
[CadQuery (version compatible with FreeCAD)](https://github.com/dcowden/cadquery)
Make sure that the cadquery repo is updated to the latest commit.

# Set-up
Open FreeCAD.
We use FreeCAD's Python console.
Add project to the FreeCAD Python path and run the requisite script.
```
import sys
sys.path.append("path_to_cadquery")
sys.path.append("path_to_repo")
import Example_002
```

To reload the script after making changes, you can type
```
reload(Example)
```
if the changes were focussed on the Example script. 
Else, you need to re-run FreeCAD.

# ProDMCAD UI
Edges can be selected using a mouse.
Pressing `f` on the keyboard performs a fillet operation on these edges.