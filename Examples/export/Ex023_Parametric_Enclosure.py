import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()

    # Parameter definitions
    vars[p_outerWidth]              = random.uniform(80.0, 120.0)  # Outer width of box enclosure
    vars[p_outerLength]             = random.uniform(130.0, 170.0)  # Outer length of box enclosure
    vars[p_outerHeight]             = random.uniform(30.0, 70.0)  # Outer height of box enclosure

    vars[p_thickness]               = random.uniform(2.8, 3.2)  # Thickness of the box walls
    vars[p_sideRadius]              = random.uniform(8.0, 12.0)  # Radius for the curves around the sides of the bo
    vars[p_topAndBottomRadius]      = random.uniform(1.8,2.2)  # Radius for the curves on the top and bottom edges

    vars[p_screwpostInset]          = random.uniform(10.0,12.0)  # How far in from the edges the screwposts should be
    vars[p_screwpostID]             = random.uniform(3.8,4.2)  # Inner diameter of the screwpost holes, should be roughly screw diameter not including threads
    vars[p_screwpostOD]             = random.uniform(8.0, 12.0)  # Outer diameter of the screwposts. Determines overall thickness of the posts

    vars[p_boreDiameter]            = random.uniform(7.8, 8.2)  # Diameter of the counterbore hole, if any
    vars[p_boreDepth]               = random.uniform(0.8, 1.2)  # Depth of the counterbore hole, if
    vars[p_countersinkDiameter]     = random.uniform(0.0, 0.1)  # Outer diameter of countersink. Should roughly match the outer diameter of the screw head
    vars[p_countersinkAngle]        = random.uniform(88.0, 92.0)  # Countersink angle (complete angle between opposite sides, not from center to one side)
    vars[p_lipHeight]               = random.uniform(0.8, 1.2)  # Height of lip on the underside of the lid. Sits inside the box body for a snug fit.

    p_outerWidth                    = vars[p_outerWidth]         
    p_outerLength                   = vars[p_outerLength]        
    p_outerHeight                   = vars[p_outerHeight]        
                                    =                             
    p_thickness                     = vars[p_thickness]          
    p_sideRadius                    = vars[p_sideRadius]         
    p_topAndBottomRadius            = vars[p_topAndBottomRadius] 
                                    =                             
    p_screwpostInset                = vars[p_screwpostInset]     
    p_screwpostID                   = vars[p_screwpostID]        
    p_screwpostOD                   = vars[p_screwpostOD]        
                                    =                             
    p_boreDiameter                  = vars[p_boreDiameter]       
    p_boreDepth                     = vars[p_boreDepth]          
    p_countersinkDiameter           = vars[p_countersinkDiameter]
    p_countersinkAngle              = vars[p_countersinkAngle]   
    p_lipHeight                     = vars[p_lipHeight]          


    # Outer shell
    oshell = cq.Workplane("XY").rect(p_outerWidth, p_outerLength) \
                                     .extrude(p_outerHeight + p_lipHeight)

    #Weird geometry happens if we make the fillets in the wrong order
    if p_sideRadius > p_topAndBottomRadius:
        oshell.edges("|Z").fillet(p_sideRadius)
        oshell.edges("#Z").fillet(p_topAndBottomRadius)
    else:
        oshell.edges("#Z").fillet(p_topAndBottomRadius)
        oshell.edges("|Z").fillet(p_sideRadius)

    # Inner shell
    ishell = oshell.faces("<Z").workplane(p_thickness, True)\
        .rect((p_outerWidth - 2.0 * p_thickness), (p_outerLength - 2.0 * p_thickness))\
        .extrude((p_outerHeight - 2.0 * p_thickness), False) # Set combine false to produce just the new boss
    ishell.edges("|Z").fillet(p_sideRadius - p_thickness)

    # Make the box outer box
    box = oshell.cut(ishell)

    # Make the screwposts
    POSTWIDTH = (p_outerWidth - 2.0 * p_screwpostInset)
    POSTLENGTH = (p_outerLength - 2.0 * p_screwpostInset)

    postCenters = box.faces(">Z").workplane(-p_thickness)\
        .rect(POSTWIDTH, POSTLENGTH, forConstruction=True)\
        .vertices()

    for v in postCenters.all():
        v.circle(p_screwpostOD / 2.0).circle(p_screwpostID / 2.0)\
         .extrude((-1.0) * ((p_outerHeight + p_lipHeight) - (2.0 * p_thickness)), True)

    # Split lid into top and bottom parts
    (lid, bottom) = box.faces(">Z").workplane(-p_thickness - p_lipHeight).split(keepTop=True, keepBottom=True).all()


    # Translate the lid, and subtract the bottom from it to produce the lid inset
    lowerLid = lid.translate((0, 0, -p_lipHeight))
    cutlip = lowerLid.cut(bottom).translate((p_outerWidth + p_thickness, 0, p_thickness - p_outerHeight + p_lipHeight))

    # Compute centers for counterbore/countersink or counterbore
    topOfLidCenters = cutlip.faces(">Z").workplane().rect(POSTWIDTH, POSTLENGTH, forConstruction=True).vertices()

    # Add holes of the desired type
    if p_boreDiameter > 0 and p_boreDepth > 0:
        topOfLid = topOfLidCenters.cboreHole(p_screwpostID, p_boreDiameter, p_boreDepth, (2.0) * p_thickness)
    elif p_countersinkDiameter > 0 and p_countersinkAngle > 0:
        topOfLid = topOfLidCenters.cskHole(p_screwpostID, p_countersinkDiameter, p_countersinkAngle, (2.0) * p_thickness)
    else:
        topOfLid= topOfLidCenters.hole(p_screwpostID, 2.0 * p_thickness)

    # Return the combined result
    result = topOfLid.combineSolids(bottom)

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
