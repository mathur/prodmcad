import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    
    vars[x] = random.uniform(2.8, 3.2)
    vars[y] = random.uniform(1.8, 2.2)
    vars[z] = random.uniform(0.3, 0.7)
    vars[s] = random.uniform(0.1, 0.3)

    x = vars[x]
    y = vars[y]
    z = vars[z]
    s = vars[s]



    result = cq.Workplane("XY").box(x,y,z).\
        faces(">Z").shell(-s).\
        faces(">Z").edges("not(<X or >X or <Y or >Y)").\
        chamfer(0.0000001)

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
