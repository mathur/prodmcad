import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()

    vars[exploded]              = False        # when true, moves the base away from the top so we see
    vars[showTop]               = True          # When true, the top is rendered.
    vars[showCover]             = True        # When true, the cover is rendered

    vars[width]                 = random.uniform(2.0, 2.2)             # Nominal x dimension of the part
    vars[height]                = random.uniform(0.4, 0.6)            # Height from bottom top to the top of the top :P
    vars[length]                = random.uniform(1.3, 1.7)            # Nominal y dimension of the part
    vars[trapezoidFudge]        = random.uniform(0.6, 0.8)    # ratio of trapezoid bases. set to 1.0 for cube
    vars[xHoleOffset]           = random.uniform(0.400, 0.500)     # Holes are distributed symmetrically about each axis
    vars[yHoleOffset]           = random.uniform(0.400, 0.600)
    vars[zFilletRadius]         = random.uniform(0.40, 0.60)    # Fillet radius of corners perp. to Z axis.
    vars[yFilletRadius]         = random.uniform(0.20, 0.30)   # Fillet radius of the top edge of the case
    vars[lipHeight]             = random.uniform(0.05, 0.15)         # The height of the lip on the inside of the cover
    vars[wallThickness]         = random.uniform(0.04, 0.8)    # Wall thickness for the case
    vars[coverThickness]        = random.uniform(0.15, 0.25)    # Thickness of the cover plate
    vars[holeRadius]            = random.uniform(0.25, 0.35)       # Button hole radius
    vars[counterSyncAngle]      = random.uniform(90, 110)  # Countersink angle.

    exploded                    = vars[exploded]        
    showTop 			= vars[showTop]         
    showCover                     vars[showCover]       
                                                        
    width 			= vars[width]           
    height 			= vars[height]          
    length 			= vars[length]          
    trapezoidFudge 		= vars[trapezoidFudge]  
    xHoleOffset 		= vars[xHoleOffset]     
    yHoleOffset 		= vars[yHoleOffset]     
    zFilletRadius 		= vars[zFilletRadius]   
    yFilletRadius 		= vars[yFilletRadius]   
    lipHeight 			= vars[lipHeight]       
    wallThickness 		= vars[wallThickness]   
    coverThickness 		= vars[coverThickness]  
    holeRadius 			= vars[holeRadius]      
    counterSyncAngle 		= vars[counterSyncAngle]

    xyplane = cq.Workplane("XY")
    yzplane = cq.Workplane("YZ")


    def trapezoid(b1, b2, h):
        "Defines a symmetrical trapezoid in the XY plane."

        y = h / 2
        x1 = b1 / 2
        x2 = b2 / 2
        return (xyplane.moveTo(-x1,  y)
                .polyline([(x1,  y),
                           (x2, -y),
                           (-x2, -y)]).close())


    # Defines our base shape: a box with fillets around the vertical edges.
    # This has to be a function because we need to create multiple copies of
    # the shape.
    def base(h):
        return (trapezoid(width, width * trapezoidFudge, length)
                .extrude(h)
                .translate((0, 0, height / 2))
                .edges("Z")
                .fillet(zFilletRadius))

    # start with the base shape
    top = (base(height)
           # then fillet the top edge
           .edges(">Z")
           .fillet(yFilletRadius)
           # shell the solid from the bottom face, with a .060" wall thickness
           .faces("<Z")
           .shell(-wallThickness)
           # cut five button holes into the top face in a cross pattern.
           .faces(">Z")
           .workplane()
           .pushPoints([(0,            0),
                        (-xHoleOffset, 0),
                        (0,           -yHoleOffset),
                        (xHoleOffset,  0),
                        (0,            yHoleOffset)])
           .cskHole(diameter=holeRadius,
                    cskDiameter=holeRadius * 1.5,
                    cskAngle=counterSyncAngle))

    # the bottom cover begins with the same basic shape as the top
    cover = (base(coverThickness)
             # we need to move it upwards into the parent solid slightly.
             .translate((0, 0, -coverThickness + lipHeight))
             # now we subtract the top from the cover. This produces a lip on the
             # solid NOTE: that this does not account for mechanical tolerances.
             # But it looks cool.
             .cut(top)
             # try to fillet the inner edge of the cover lip. Technically this
             # fillets every edge perpendicular to the Z axis.
             .edges("#Z")
             .fillet(.020)
             .translate((0, 0, -0.5 if exploded else 0)))
    # Conditionally render the parts
    if showTop:
       result=top
    if showCover:
        result=cover
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
