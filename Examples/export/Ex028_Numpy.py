import numpy as np

import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    
    vars[side]          = random.uniform(8,12)
    offset[side]        = random.uniform(3,7)

    # Square side and offset in x and y.
    side                = vars[side]
    offset              = vars[offset]

    # Define the locations that the polyline will be drawn to/thru.
    # The polyline is defined as numpy.array so that operations like translation
    # of all points are simplified.
    pts = np.array([
        (0, 0),
        (side, 0),
        (side, side),
        (0, side),
        (0, 0)
    ]) + [offset, offset]

    result = cq.Workplane('XY') \
        .polyline(pts).close().extrude(2) \
        .faces('+Z').workplane().circle(side / 2).extrude(1)
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
