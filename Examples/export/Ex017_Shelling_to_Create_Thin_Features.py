import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    vars[x] = random.uniform(2.8, 3.2)
    vars[y] = random.uniform(1.8, 2.2)
    vars[z] = random.uniform(0.3, 0.7)
    vars[s] = random.uniform(0.03, 0.07)

    # Create a hollow box that's open on both ends with a thin wall.
    # 1.  Establishes a workplane that an object can be built on.
    # 1a. Uses the named plane orientation "front" to define the workplane, meaning
    #     that the positive Z direction is "up", and the negative Z direction
    #     is "down".
    # 2.  Creates a plain box to base future geometry on with the box() function.
    # 3.  Selects faces with normal in +z direction.
    # 4.  Create a shell by cutting out the top-most Z face.
    result = cq.Workplane("front").box(vars[x], vars[y], vars[z]).faces("+Z").shell(vars[s])

    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
