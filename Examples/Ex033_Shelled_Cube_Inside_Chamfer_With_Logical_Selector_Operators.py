# Example using advanced logical operators in string selectors to select only
# the inside edges on a shelled cube to chamfer.
import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

result = cq.Workplane("XY").box(2, 2, 2).\
    faces(">Z").shell(-0.2)#.\
    #faces(">Z").edges("not(<X or >X or <Y or >Y)").\
    #chamfer(0.0000001)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Render the solid
Part.show(result.toFreecad())

print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
