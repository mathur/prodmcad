# Inspired by the design at: https://www.thingiverse.com/thing:3819146/files

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("ParametricHoseAdapter")
# [Basics]

# Inner Diameter of Part A (mm)
part_a_inner_diameter = 32

# Outer Diameter of Part B (mm)
part_b_outer_diameter = 37

# [Advanced]

# Material Strength (mm)
material_strength = 4

# How long is the slide on section per part? (mm)
slide_on_length = 20

# How long is the connection section between the parts? (mm)
connection_length = 10

# What is the slide on ramp for creating a tight fit? (mm)
slide_on_ramp = 2

def adapter(partA, partB, strength, length, con_length, ramp):

    # Part B
    part_b = cq.Workplane("XY").circle((partB + strength)/2.0).extrude(length)
    part_b_cut = cq.Workplane("XY").circle((partB + ramp) / 2.0).workplane(offset=length).circle((partB - ramp) / 2.0).loft()
    part_b = part_b.cut(part_b_cut)

    # Connection between Part A and Part B
    part_ab_workplane = part_b.faces(">Z").workplane()
    part_ab = part_ab_workplane.circle((partB + strength) / 2.0).workplane(offset=con_length).circle((partA + ramp) / 2.0).loft(combine=False)
    part_ab_cut = part_ab_workplane.circle((partB - ramp) / 2.0).workplane(offset=con_length).circle((partA - strength) / 2.0).loft(combine=False)
    part_ab = part_ab.cut(part_ab_cut)

    # Part A
    part_a_workplane = part_ab.faces(">Z").workplane()
    part_a = part_a_workplane.circle((partA + ramp) / 2.0).workplane(offset=length).circle((partA - ramp) / 2.0).loft(combine=False)
    part_a_cut = part_ab.faces(">Z").workplane().circle((partA - strength)/2.0).workplane(offset=length).circle((partA-strength)/2).loft(combine=False)
    part_a = part_a.cut(part_a_cut)

    return part_b.union(part_ab.union(part_a))

result = adapter(part_a_inner_diameter,
    part_b_outer_diameter,
    material_strength,
    slide_on_length,
    connection_length,
    slide_on_ramp)

# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\ParametricHoseAdapter.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()