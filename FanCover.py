# Inspired by the customizable fan cover at: https://www.thingiverse.com/thing:735449/

# Import statements
import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("FanCover")
# Fan cover size (always square)
fancoversize = 4
# Fan cover height
fancoverheight = .2

# Screws inside offset from corners
screwpos = .35

# Size of screw hole
screwsize = .3

# Size of screw head
screwcoversize = .55

# Number of concentric circles
circles = 4
# Number of radial supports
nradis = 3
# 0-1 Percent of empty space for air.
percair = .75

# Inset fan
inh = .06

fancoversize = fancoversize * 1.0

ncircles = circles+1
conerdelta = (fancoversize/2 - screwpos) *1.0

stepsize = (fancoversize/2) / (1.0 * ncircles)
fillstepsize = stepsize*percair
emptystepsize = stepsize*(1-percair)

"""
# Make concentric rings
rings = cq.Workplane("XY").transformed(offset=(0,0,-fancoverheight/2))

for i in range(2, ncircles):
    rad_1 = (stepsize*i-emptystepsize/2)
    rad_2 = (stepsize*(i-1)+emptystepsize/2)
    if rad_1 > 0 or rad_2 > 0:
        cyl1 = rings.circle(radius=rad_1).extrude(distance=fancoverheight*2, combine=False)
        cyl2 = rings.transformed(offset=(0,0,-.1)).circle(radius=rad_2).extrude(distance=fancoverheight*2, combine=False)
        rings = rings.union(cyl1.cut(cyl2))
"""

# Attempt to remake the model a little differently from the OpenSCAD version
# Create the base
fan = cq.Workplane("XY").box(fancoversize, fancoversize, fancoverheight)
# Smooth the edges of the base
fan_smooth = fan.edges("|Z").fillet(screwcoversize)
# Make the cbore holes
fan_smooth = fan_smooth.faces(">Z").rect(xLen=fancoversize - (conerdelta/2), yLen=fancoversize - (conerdelta/2), forConstruction=True)\
    .vertices().cboreHole(diameter=screwsize, cboreDiameter=screwcoversize, cboreDepth=fancoverheight/2)

# Now we make the cut circles
rings = cq.Workplane("XY")
for i in range(2, ncircles + 1):
    cyl_1 = fan_smooth.faces(">Z").workplane(offset=(-fancoverheight)).circle(radius=stepsize*i-emptystepsize/2).extrude(distance=fancoverheight*2, combine=False)
    cyl_2 = fan_smooth.faces(">Z").workplane(offset=(-fancoverheight)).circle(radius=stepsize*(i-1)+emptystepsize/2).extrude(distance=fancoverheight*2, combine=False)
    rings = rings.union(cyl_1.cut(cyl_2))

fan = fan_smooth.cut(rings)
# Now we make the radial supports
rads = cq.Workplane("XY")
for i in range(0,nradis):
    fan = fan.faces(">Z").workplane().transformed(rotate=(0,0, 360.0 /nradis*i), offset=(0.0,-emptystepsize/2.0,-fancoverheight/2)).box(fancoversize,emptystepsize,fancoverheight)

# Some extra stuff from OpenSCAD

# Included at time of generation of rings
if inh != 0:
    inh_shape = fan.faces("<Z").circle(radius=(fancoversize/2.0)-(emptystepsize/2.0)).extrude(inh-0.01, combine=False)
fan = fan.cut(inh_shape)

# Export to FreeCAD
# Part.show(fan.toFreecad())
# Export STL
f = open('Exports\\FanCover.stl', 'w')
cq.exporters.exportShape(shape=fan, fileLike=f, exportType="STL",tolerance=0.01)
f.close()
