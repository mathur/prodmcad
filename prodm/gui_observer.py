import FreeCAD
import FreeCADGui  # To use selection
import Part
import cadquery
import comparator
import synthesize
import shape_analyzer
import freecad_ops
import file_writer
import inspect
from collections import OrderedDict
import time
# Copying to clipboard
import pyperclip

class ViewObserver(object):
    """
    This is the class for listening to GUI events in the FreeCAD interface
    """

    def __init__(self, tracker, obj, var_dict, verbose=False):
        """
        Sets up the view observer object
        :param tracker: the shape_analyzer.Tracker instance tracking all changes.
        obj: the cadquery object
        var_dict: variables in the script
        verbose: verbosity of the observation process.
        """
        self.tracker = tracker
        self.obj = obj
        self.var_dict = var_dict
        # Clean the variable dictionary (remove non-float vals)
        self.clean_var_dict()
        self.verbose = verbose

    def get_dependence(self, track_id, line_num):
        """
        Given line number, gives the object created and source object in the track file
        :param track_id:
        :param line_num:
        :param obj_id:
        :return:
        """
        # Get src_ids by look up in dict structure
        src_ids = track_id[line_num].src_ids
        line_num_src = None
        for key, val in track_id.items():
            if key == line_num:
                # The loop breaks after reaching the line number in question
                break
            if val.obj_id in src_ids:
                # Keeps the last value
                line_num_src = key
        return src_ids, line_num_src

    def get_selection(self, shape="Unknown"):
        """
        Get selections using FreeCAD interface, finding in the process
        :param: shape: the shape of selected objects requested
                        Can be "Edge", "Vertex", "Face".
        :return:
        """
        selection = FreeCADGui.Selection.getSelectionEx()

        selected = []
        selected_freecad = []
        line_nums_selected = []

        # If the shape type is not known
        if shape not in ["Edge", "Vertex", "Face"]:
            # Find type
            for o in selection:
                if o.HasSubObjects:
                    for obj in o.SubObjects:
                        # If it is of the specific type
                        shape = obj.ShapeType
                        break

        # What is selected?
        for o in selection:
            if o.HasSubObjects:
                for obj in o.SubObjects:
                    # If it is of the specific type
                    if obj.ShapeType == shape:
                        selected_freecad.append(obj)
                        # Cast properly to cq object
                        cq_obj = None
                        if shape == "Edge":
                            cq_obj = cadquery.Edge.cast(obj)
                        elif shape == "Face":
                            cq_obj = cadquery.Face.cast(obj)
                        elif shape == "Vertex":
                            cq_obj = cadquery.Vertex.cast(obj)
                        else:
                            print("Error! Unknown type encountered.")
                            return
                        # Iterate over dictionary entries of the line-by-line tracker
                        found = False
                        for key, val in self.tracker.track.items():
                            print("Looking for object in line", key)
                            if val.pending is False:
                                # If this entry does not include anything intermediate
                                for tracked_obj in val.object_desc:
                                    # For each face or edge on this line number
                                    entry_list = None
                                    if shape == "Edge":
                                        entry_list = tracked_obj.edges
                                    elif shape == "Face":
                                        entry_list = tracked_obj.faces
                                    elif shape == "Vertex":
                                        entry_list = tracked_obj.vertices
                                    for entry in entry_list:
                                        # Check if objects match
                                        if comparator.Comparator.compare_shape(entry, cq_obj):
                                            print("Found in", key)
                                            selected.append(entry)
                                            line_nums_selected.append(key)
                                            found = True
                                            break
                                    if found is True:
                                        break
                                if found is True:
                                    break

        return selected_freecad, selected, line_nums_selected

    def operation(self, time_keeping=True):
        """
        Synthesizes query for a generic operation.
        It automatically infers the type of elements selected.
        :param time_keeping:
        :return:
        """
        # Make aggregated lists
        agg = shape_analyzer.Aggregates(self.obj, verbose=False)
        # If button is pressed, we try to find selected objects
        selected_freecad, selected_elems, line_nums_selected = self.get_selection()

        print("Synthesis: using aggregated lists and decision tree (adaptive)")
        if selected_elems is not []:
            formula = synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_elems, cad_obj=self.obj,
                                                                    mode=2)
        ViewObserver.print_formula(formula=formula)

    def edge_operation(self, operation, arg=0.125, time_keeping=True):
        """
        Does edge related CAD operations
        :param operation:
        :param arg:
        :return:
        """
        # Make aggregated lists
        agg = shape_analyzer.Aggregates(self.obj, verbose=False)

        # If button is pressed, we try to find selected objects
        selected_edges_freecad, selected_edges, line_nums_selected = self.get_selection(shape="Edge")

        if line_nums_selected is not []:
            print("Line numbers with this shape", line_nums_selected)

            # Find the earliest line number with all edges in selected_edges
            line_sel = min(line_nums_selected)
            print("The required line number is", line_sel)

            # Print dependence
            src_id, line_src = self.get_dependence(self.tracker.track_id, line_sel)
            print("The object is created from", src_id, " which occurs in line", line_src)

        # Generate a bounding box for the selected edges
        """
        print("Synthesis: building a bounding box...")
        start_time = time.time()
        synthesize.Selector.synthesize_bounding_box(target_list=selected_edges, cad_obj=self.obj)
        if time_keeping:
            print("Bounding box selection took ", time.time() - start_time)

        start_time = time.time()
        print("Synthesis: using local variables to build bounding box")
        synthesize.Selector.synthesize_bounding_box_var(target_list=selected_edges, cad_obj=self.obj,
                                                        var_dict=self.var_dict)
        if time_keeping:
            print("Bounding box with var selection took ", start_time - time.time())

        print("Synthesis: using aggregated lists and a decision tree")
        synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_edges, cad_obj=self.obj, verbose=True)
        """
        print("Synthesis: using aggregated lists and decision tree (adaptive)")
        formula = synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_edges, cad_obj=self.obj,
                                                                mode=2, verbose=False)

        ViewObserver.print_formula(line_sel=line_sel, element_type="edges", formula=formula, operation=operation)

    def face_operation(self, arg=None, operation="bore", time_keeping=True):
        """
        Perform operation involving faces
        :return:
        """

        # If button is pressed, we try to find selected objects
        selected_faces_freecad, selected_faces, line_nums_selected = self.get_selection(shape="Face")

        if line_nums_selected is not None:
            print("Line numbers with this shape", line_nums_selected)

            # Find the earliest line number with all edges in selected_edges
            line_sel = min(line_nums_selected)
            print("The required line number is", line_sel)

            # Print dependence
            src_id, line_src = self.get_dependence(self.tracker.track_id, line_sel)
            print("The object is created from", src_id, " which occurs in line", line_src)

        # Synthesis process starts here
        print("Synthesis: using aggregated lists and decision tree (adaptive)")
        formula = synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_faces, cad_obj=self.obj,
                                                                mode=2)
        ViewObserver.print_formula(line_sel=line_sel, element_type="faces", formula=formula, operation=operation)

    @classmethod
    def print_formula(cls, formula, element_type=None, line_sel=None, operation=None):
        """
        Prints the formula in a dialog
        :param element_type: The type of the element (edges, vertices, faces)
        :param formula: The selection query synthesized
        :param operation: The CAD operation to be performed
        :return:
        """
        # TODO: Write a better pretty print function
        msg = "Selection Query: " + formula
        if line_sel is not None:
            msg = msg + "\nLine number: " + str(line_sel)
        if operation is not None:
            msg = msg + "\nOperation: " + operation
        msg = msg + "\nCopied to clipboard!"
        # Copy the query to the clipboard
        pyperclip.copy(formula)
        freecad_ops.post_message(msg=msg)

    def vertex_operation(self, arg=None, operation="countersink_hole", time_keeping=True):
        """
        Opertations of vertices, called from by the keyboard event listener
        :param arg: Argument to the operation
        :param operation: Operation to be performed
        :return:
        """
        print("Vertex operation")
        selected_vertices_freecad, selected_vertices, line_nums_selected = self.get_selection(shape="Vertex")
        print("We selected the vertices", selected_vertices)

        # Perform the operation

        # Synthesis process

        # Find the earliest line number with all shapes in selected_shapes
        line_sel = min(line_nums_selected)
        print("The required line number is", line_sel)

        start_time = time.time()
        print("Synthesis: building a bounding box...")
        synthesize.Selector.synthesize_bounding_box(target_list=selected_vertices, cad_obj=self.obj)
        if time_keeping:
            print("Bounding box selection took ", time.time() - start_time)

        start_time = time.time()
        print("Synthesis: using local variables to build bounding box")
        synthesize.Selector.synthesize_bounding_box_var(target_list=selected_vertices, cad_obj=self.obj,
                                                        var_dict=self.var_dict)
        if time_keeping:
            print("Bounding box with var selection took ", start_time - time.time())

        synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_vertices, cad_obj=self.obj)

        print("Synthesis: using aggregated lists and decision tree (chaining)")
        synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_vertices, cad_obj=self.obj,
                                                      mode=1)

        print("Synthesis: using aggregated lists and decision tree (adaptive)")
        formula = synthesize.Selector.synthesize_from_agg_dtree(target_list=selected_vertices, cad_obj=self.obj,
                                                                mode=2)
        freecad_ops.post_message(msg=formula)

    def keyboard(self, info):
        # Check if a certain key was pressed---these represent user request to perform a cad operation
        # F or f for fillet operation on edges
        f_down = (info["Key"] == "F" or info["Key"] == "f")
        # C or c for chamfer operation on edges
        c_down = (info["Key"] == "C" or info["Key"] == "c")
        t_down = (info["Key"] == "T" or info["Key"] == "t")
        # B or b for bore hole operation on faces
        b_down = (info["Key"] == "B" or info["Key"] == "b")
        # S or s for shell operation on faces
        s_down = (info["Key"] == "S" or info["Key"] == "s")
        # countrer-sink or counterbore hole operation
        q_down = (info["Key"] == "Q" or info["Key"] == "q")
        # M or m for generic operation
        m_down = (info["Key"] == "M" or info["Key"] == "m")

        if m_down:
            self.operation()

        elif f_down:
            self.edge_operation(operation="fillet")

        elif c_down:
            self.edge_operation(operation="chamfer")

        elif t_down:
            """
            Prints out when selected edges were created in code
            """
            # If button is pressed, we try to find selected objects
            selected_edges_freecad, selected_edges, line_nums_selected = self.get_selection()

            # We assume selected_edges[0], line_nums_selected[0]
            line_num = line_nums_selected[0]
            selected_edge = selected_edges[0]
            print("We assume the first selection (in case of multiple), line num", line_num)

            # Get list of suggested edges
            suggested_edges = []

            # Get all objects at the selected line number
            objs = shape_analyzer.get_objects_at_line(self.tracker.track, line_num)

            # Get index of objs where the selected_edges[0] exists
            id_selected = shape_analyzer.get_index_edge_in_objects(tracked_objects=self.tracker.track[line_num],
                                                                   edge=selected_edge)

            # Generate selector for selected edge and sub-object
            synthesize.Selector.synthesize_from_agg_dtree(target_list=[selected_edge], cad_obj=objs[id_selected])

            print("Operation will not be performed on FreeCAD- not yet implemented.")
            # freecad_ops.make_fillet(selected_edges_cq=suggested_edges)

        elif b_down:
            """
            Operation on faces- bore a hole
            """
            self.face_operation(arg=None, operation="bore")

        elif s_down:
            """
            Operation on faces- shell a face
            """
            self.face_operation(arg=None, operation="shell")

        elif q_down:
            """
            Operation on vertices
            """
            print("Operation on vertices requested")
            self.vertex_operation(arg=None, operation="countersink_hole")

    def clean_var_dict(self):
        """
        Cleans the var_dict member by only including float members, adding a very large number, adding negatives of vars
        :return: a sorted ordered dictionary
        """
        # Set cleaned_dict to be empty dict
        cleaned_dict = OrderedDict()
        self.var_dict["_INF_"] = 9999.9
        # For each value
        for key, value in self.var_dict.iteritems():
            if isinstance(value, float):
                # Include this if it is a float
                cleaned_dict[key] = value
                cleaned_dict["-" + key] = -value

        # Set ref to var_dict
        self.var_dict = OrderedDict(sorted(cleaned_dict.iteritems(), key=lambda x: x[1]))


class ViewListener(object):
    def __init__(self, tracker, obj, show_obj=False):
        """
        Initializes the object
        :param tracker: the track file as per shape_analyzer.Tracker
        :param obj: the cadquery object
        """
        if show_obj:
            Part.show(obj.toFreecad())

        self.view = FreeCADGui.activeDocument().activeView()
        # Set view to Axonometric
        self.view.viewAxonometric()
        # Fit the view
        FreeCADGui.SendMsgToActiveView("ViewFit")
        # Get local variables from program frame
        curr_frame = inspect.currentframe()
        try:
            var_dict = curr_frame.f_back.f_locals
        finally:
            del curr_frame

        self.obs = ViewObserver(tracker, obj, var_dict=var_dict)




    """
    def start_mouse(self):
        c = self.view.addEventCallback("SoMouseButtonEvent", self.obs.selected_object)
    """

    def start_keyboard(self):
        k = self.view.addEventCallback("SoKeyboardEvent", self.obs.keyboard)
        raw_input("Press Enter to continue...")
