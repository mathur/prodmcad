import FreeCAD
import Part
import comparator
import cadquery
from PySide import QtCore, QtGui


def make_fillet(selected_edges_freecad=None, selected_edges_cq=None, argument=0.5):
    """
    Performs the fillet operation on the list of edges for the main object in document
    :param selected_edges_freecad: list of edges to perform fillet on
    :return:
    """
    obj_freecad = FreeCAD.ActiveDocument.Objects[0].Shape

    if selected_edges_cq is None:
        print(selected_edges_freecad)

    elif selected_edges_freecad is None:
        selected_edges_freecad = []
        # Get edges from FreeCAD
        for obj in obj_freecad.Edges:
            # obj needs to be cast into cadquery
            cq_obj = cadquery.Edge.cast(obj)
            for edges in selected_edges_cq:
                if comparator.Comparator.compare_shape(edges, cq_obj):
                    selected_edges_freecad.append(obj)
                    break
        print(selected_edges_freecad)

    obj_freecad = obj_freecad.makeFillet(argument, selected_edges_freecad)
    # Show the final object
    myDocument = FreeCAD.newDocument("Filleted")
    Part.show(obj_freecad)


def make_chamfer(selected_edges_freecad=None, selected_edges_cq=None, argument=0.5):
    """
    Performs the chamfer operation on the list of edges for the main object in document
    :param selected_edges_freecad: list of edges to perform chamfer on
    :return:
    """
    obj_freecad = FreeCAD.ActiveDocument.Objects[0].Shape

    if selected_edges_cq is None:
        print(selected_edges_freecad)

    elif selected_edges_freecad is None:
        selected_edges_freecad = []
        # Get edges from FreeCAD
        for obj in obj_freecad.Edges:
            # obj needs to be cast into cadquery
            cq_obj = cadquery.Edge.cast(obj)
            for edges in selected_edges_cq:
                if comparator.Comparator.compare_shape(edges, cq_obj):
                    selected_edges_freecad.append(obj)
                    break
        print(selected_edges_freecad)

    obj_freecad = obj_freecad.makeChamfer(argument, selected_edges_freecad)

    # Show the final object
    Part.show(obj_freecad)


def make_bore(selected_faces_freecad=None, selected_faces_cq=None, radius=0.2, height=1.0):
    """
    Bores hole of diameter=10 on the list of faces
    :param selected_faces_freecad: list of faces
    :return:
    """
    obj_freecad = FreeCAD.ActiveDocument.Objects[0].Shape

    if selected_faces_cq is None:
        print(selected_faces_freecad)

    elif selected_faces_freecad is None:
        selected_faces_freecad = []
        # Get edges from FreeCAD
        for obj in obj_freecad.Faces:
            # obj needs to be cast into cadquery
            cq_obj = cadquery.Face.cast(obj)
            for edges in selected_faces_cq:
                if comparator.Comparator.compare_shape(edges, cq_obj):
                    selected_faces_freecad.append(obj)
                    break
        print(selected_faces_freecad)

    cyl_freecad = Part.makeCylinder(radius, height)

    for face in selected_faces_freecad:
        face = face.cut(cyl_freecad)

    print("Warning! This operation has not been implemented yet!")
    Part.show(obj_freecad)


def make_shell(selected_faces_freecad=None, selected_faces_cq=None, argument=-0.2):
    """
    Bores hole of diameter=10 on the list of faces
    :param selected_faces_freecad: list of faces
    :return:
    """
    obj_freecad = FreeCAD.ActiveDocument.Objects[0].Shape

    if selected_faces_cq is None:
        print(selected_faces_freecad)

    elif selected_faces_freecad is None:
        selected_faces_freecad = []
        # Get edges from FreeCAD
        for obj in obj_freecad.Faces:
            # obj needs to be cast into cadquery
            cq_obj = cadquery.Face.cast(obj)
            for edges in selected_faces_cq:
                if comparator.Comparator.compare_shape(edges, cq_obj):
                    selected_faces_freecad.append(obj)
                    break
        print(selected_faces_freecad)
    if argument is None:
        argument = -0.5
    obj_freecad = obj_freecad.makeThickness(selected_faces_freecad, argument, 0.0001)
    Part.show(obj_freecad)


def post_message(header="Formula generated", msg=""):
    """
    Posts a GUI message in FreeCAD
    :param header:
    :param msg:
    :return:
    """

    # The first argument indicates the icon used: one of QtGui.QMessageBox.
    # {NoIcon, Information, Warning, Critical, Question}
    diag = QtGui.QMessageBox(QtGui.QMessageBox.NoIcon, header, msg)
    diag.setWindowModality(QtCore.Qt.ApplicationModal)
    diag.exec_()
