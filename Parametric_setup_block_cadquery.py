'''
Parametric 123 Block
Link to similar Thingiverse file: https://www.thingiverse.com/thing:3749463/files
'''

import cadquery as cq
import random
import json

# Number of iterations for the model
num_iter = 50

print("Setup Block")


''' Parameters '''

moduleMajorDimension = 25.4  # Default: 25.4
holeSizeFraction = 0.5         # Default: .5
ecks = 2   # Default: 2
wye = 1    # 1;
zed = 3    # 3;


def single_block():
    """
    Construct singular blocks.
    :return: cq Solid
    """
    block = cq.Workplane("XY").box(moduleMajorDimension, moduleMajorDimension, moduleMajorDimension)
    block = block.faces("<X").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension+0.01)
    block = block.faces("<Y").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension + 0.01)
    block = block.faces("<Z").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension + 0.01)
    sphere_cut = cq.Workplane("XY").sphere(radius=moduleMajorDimension*holeSizeFraction*1.414/2.0)
    result = block.cut(sphere_cut)
    return result

result = cq.Workplane("XY")
for x in range(ecks):
    for y in range(wye):
        for z in range(zed):
            result_temp = single_block().translate(vec=((moduleMajorDimension*x, moduleMajorDimension*y, moduleMajorDimension*z)))
            result = result.union(result_temp)
# Export STL
f = open('Exports\\SetupBlock.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()