#!/usr/bin/env python3

import os
import re
import numpy as np
import meshlabxml as mlx
import sys
import time

MESHLABSERVER_PATH = 'C:\\Program Files\\VCG\\MeshLab'
os.environ['PATH'] += os.pathsep + MESHLABSERVER_PATH

num_iter = 50


def diff_bb( file1, file2, log):
    """Calculate diffs and the hausdorf distance"""
    # Calculate difference for each iteration
    for iter in range(num_iter):
        diff = mlx.FilterScript(file_in=[file1, file2])
        diff.parse_hausdorff = True
        mlx.sampling.hausdorff_distance(script=diff)

        diff.run_script(log=log_file)

        geom = mlx.FilterScript(file_in=file2)
        mlx.compute.measure_geometry(geom)
        geom.run_script(log=log_file)
        print("Completed iteration: ", iter)
    return None

def clean_log( log_file, rms_file, bb_file, rms_ratio_log ):
    # Clean the file and save the RMS
    # Regex to extract rms data
    # I make the numpy arrays, but do not use them
    rms_data = re.compile("rms_distance *= *(.*)")
    rms_np = np.array([])
    with open(log_file, "r") as fh:
        data = fh.read()
        rms = rms_data.findall(data)
        print("Mean squared errors extracted.")
        with open( rms_file, "w+") as summary_file:
            for iter in range(num_iter):
                summary_file.write(rms[iter] + "\n")
                rms_np = np.append(rms_np, float(rms[iter]))

    bb_data = re.compile("aabb_diag *= *(.*)")
    bb_np = np.array([])
    with open(log_file, "r") as fh:
        data = fh.read()
        bb = bb_data.findall(data)
        print("Bounding box data extracted.")
        with open( bb_file, "w+") as summary_file:
        for iter in range(num_iter):
            summary_file.write(bb[iter] + "\n")
            bb_np = np.append(bb_np, float(bb[iter]))

    rms_ratio = np.array([])
    for i in range(len(rms_np)):
        rms_ratio = np.append(rms_ratio, rms_np[i] / bb_np[i])

    print("RMS ratios are ", rms_ratio)
    print("Max is ", np.max(rms_ratio))
    print ("Average is ", np.mean(rms_ratio))
    f_summary = open("log_rms_ratio.txt", "w+")
    with open(rms_ratio_log, "w+") as fh:
    for iter in range(len(rms_ratio)):
        fh.write(str(rms_ratio[iter]) + "\n")

def main():
    hash            =  str(hex(int(time.time())))
    log_file        = "log_"+str(file1)+"-"+str(file2)+"_"+hash
    rms_file        = "log_rms_"+hash
    bb_file         = "log_bb_"+hash
    rms_ratio_log   = "log_rms_ratio_"+hash
    file1           = sys.argv[1]
    file2           = sys.argv[2]
    
    print( "Comparing %s with %s" %( str(file1), str(file2) ) )
    diff_bb(file1, file2, log_file)
    clean_log(log_file, rms_file, bb_file, rms_ratio_log)

if __name__ == '__main__':
    main()
