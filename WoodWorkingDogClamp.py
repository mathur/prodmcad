# Inspired by the model at: https:# www.thingiverse.com/thing:3839155
import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

DogsHoleType=0	# 0=round 1=square
DogsHoleDiameter=15 # In case of square type, this is the side measure
DogLength=20

CenterHoleDiameter=6.25
NutRecessSize=11

Part=0

if Part == 1 or Part == 0:
    if DogsHoleType == 0:
        part_a = cq.Workplane("XY").circle((DogsHoleDiameter-2)/2.0).workplane(offset=DogLength)\
            .circle(DogsHoleDiameter/2.0).loft()
    if DogsHoleType == 1:
        part_b = cq.Workplane("XY").box(DogsHoleDiameter, DogsHoleDiameter, DogLength)
    # Bottom part
    part_c = cq.Workplane("XY").workplane(offset=-DogLength/2.0).circle(DogsHoleDiameter-1.0)\
            .workplane(offset=2.0).circle(DogsHoleDiameter-3.0).loft()
