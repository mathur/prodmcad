$fn=360; //faces, 1 per degree tends to give a good round surface

inner_diameter=25.4*(1+1/4)+0; //+0 adjustment factor to account for 
outer_diameter=25.4*(1+3/8)+0;

height=30; //in mm
screw_hole_from_top=6; // in mm
screw_hole_border=6; // in mm
screw_hole_depth=5; //how deep is the screw
screw_size=2.5; //M3-0.5 //Screw size-pitch To allow for tapping. 
// If you want to use a nut instead of tapping, set screw_nut equal to 6, screw_nut_depth, and screw_nut_head. Set the screw size to full diameter, and also make sure screw_hole_border, and screw_hole_depth are large enout
screw_nut=360; 
screw_nut_depth=1; 
screw_nut_head=4; //tip to tip, not flat to flat. So generally the wrench size *sqrt(2)

difference(){
	union(){
		cylinder(d=outer_diameter, h=height);
		for(i=[0:3]) {
			rotate([0,0,i*120]) {
				translate([-inner_diameter/2-screw_hole_depth,-screw_hole_border/2,height-screw_hole_from_top]) {
					cube([outer_diameter/2+screw_hole_depth,screw_hole_border, screw_hole_border ]);
				}
			}
		}
	}
	
	//cutouts!
	translate([0,0,-0.1]) //Eliminates artifacts in preview
		cylinder(d=inner_diameter, h=height+0.2);
	for(i=[0:3]) {
			rotate([0,0,i*120]) {
				translate([-inner_diameter/2-screw_hole_depth-0.1,0,height-screw_hole_from_top/2]) {
					rotate([0,90,0])
						if (screw_nut != 6) {
							cylinder(h=inner_diameter, d=screw_size, $fn=screw_nut);
						} else {
							cylinder(h=inner_diameter, d=screw_size);
							cylinder(h=screw_nut_depth, d=screw_nut_head, $fn=screw_nut);
							
						}
				}
			}
		}
	
}