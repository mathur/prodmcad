import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()


inner_diameter = 25.4 * (1 + 1 / 4.0) + 0  # +0 adjustment factor to account for
outer_diameter = 25.4 * (1 + 3 / 8.0) + 0
screw_size = 2.5

screw_hole_border = 6

screw_hole_depth = 5

height = 30

screw_hole_from_top = 6

#  If you want to use a nut instead of tapping, set screw_nut equal to 6, screw_nut_depth, and screw_nut_head. Set the screw size to full diameter, and also make sure screw_hole_border, and screw_hole_depth are large enout
screw_nut = 360

screw_nut_depth = 1 

screw_nut_head = 4

base = cq.Workplane("XY").circle(outer_diameter/2.0).extrude(height)
base_workplane = base.faces(">Z").workplane()
hole_base = cq.Workplane("XY")
for i in range(0, 3):
    temp = base_workplane.transformed(rotate=(0,0,i*120))\
        .transformed(offset=((-inner_diameter/2.0)-screw_hole_depth, -screw_hole_border/2.0, -screw_hole_from_top))\
        .box(outer_diameter/2.0 + screw_hole_depth, screw_hole_border, screw_hole_border, combine=False, centered=(False, False, False))

    hole_base = hole_base.union(temp)

result = base.union(hole_base)
# Hole with inner diameter
# Removed depth
result.faces(">Z").hole(diameter=inner_diameter)
# Nut cut out (parameterized cut shape)
base_workplane = base.faces(">Z").workplane()
cuts = cq.Workplane("XY")
for i in range(0, 3):
    temp = base_workplane.transformed(offset=(0, 0, -screw_hole_from_top/2.0)) \
            .transformed(rotate=(0, -90, i * 120)) \
    # We assume it is either hex or circular
    if screw_nut != 6:
        temp = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
    else:
        temp_1 = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
        temp_2 = temp.polygon(nSides=screw_nut, diameter=screw_nut_head).extrude(screw_nut_depth, combine=False)
        temp = temp_1.union(temp_2)
    cuts = cuts.union(temp)
result = result.cut(cuts)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Return the block
Part.show(result.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, block_xyz)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, block)
listener.start_keyboard()
