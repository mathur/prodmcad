# Inspired by the model at: thingiverse.com/thing:3811422

import cadquery as cq
import random
import json

print("EyepieceHolder")

# Number of iterations
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    inner_diameter = 25.4 * (1 + 1 / 4.0) + 0  # +0 adjustment factor to account for
    outer_diameter = 25.4 * (1 + 3 / 8.0) + 0
    screw_size = random.choice([2.5, 0.5])  # M3-0.5 # Screw size-pitch To allow for tapping, default: 2.5

    screw_hole_border = random.uniform(screw_size, 10*screw_size)  # in mm, default: 6
    vars["screw_hole_border"] = str(screw_hole_border)

    screw_hole_depth = random.uniform(0, outer_diameter - inner_diameter)  # how deep is the screw, default: 5
    vars["screw_hole_depth"] = str(screw_hole_depth)

    height = random.uniform(2*screw_hole_border, 100)  # in mm, default: 30
    vars["height"] = str(height)

    screw_hole_from_top = random.uniform(2*screw_hole_border, height-screw_hole_border)  # in mm, default: 6
    vars["screw_hole_from_top"] = str(screw_hole_from_top)

    #  If you want to use a nut instead of tapping, set screw_nut equal to 6, screw_nut_depth, and screw_nut_head. Set the screw size to full diameter, and also make sure screw_hole_border, and screw_hole_depth are large enout
    screw_nut = random.choice([360, 6])  # Default: 360
    vars["screw_nut"] = str(screw_nut)

    screw_nut_depth = random.uniform(0, outer_diameter - inner_diameter)  # Default: 1
    vars["screw_nut_depth"] = str(screw_nut_depth)

    screw_nut_head = random.uniform(0, screw_hole_border)  # tip to tip, not flat to flat. So generally the wrench size *sqrt(2), default: 4
    vars["screw_nut_head"] = str(screw_nut_head)

    base = cq.Workplane("XY").circle(outer_diameter/2.0).extrude(height)
    base_workplane = base.faces(">Z").workplane()
    hole_base = cq.Workplane("XY")
    for i in range(0, 3):
        temp = base_workplane.transformed(rotate=(0,0,i*120))\
            .transformed(offset=((-inner_diameter/2.0)-screw_hole_depth, -screw_hole_border/2.0, -screw_hole_from_top))\
            .box(outer_diameter/2.0 + screw_hole_depth, screw_hole_border, screw_hole_border, combine=False, centered=(False, False, False))

        hole_base = hole_base.union(temp)

    result = base.union(hole_base)
    # Hole with inner diameter
    # Removed depth
    result.faces(">Z").hole(diameter=inner_diameter)
    # Nut cut out (parameterized cut shape)
    base_workplane = base.faces(">Z").workplane()
    cuts = cq.Workplane("XY")
    for i in range(0, 3):
        temp = base_workplane.transformed(offset=(0, 0, -screw_hole_from_top/2.0)) \
                .transformed(rotate=(0, -90, i * 120)) \
        # We assume it is either hex or circular
        if screw_nut != 6:
            temp = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
        else:
            temp_1 = temp.circle(screw_size/2.0).extrude(inner_diameter, combine=False)
            temp_2 = temp.polygon(nSides=screw_nut, diameter=screw_nut_head).extrude(screw_nut_depth, combine=False)
            temp = temp_1.union(temp_2)
        cuts = cuts.union(temp)
    result = result.cut(cuts)
    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()