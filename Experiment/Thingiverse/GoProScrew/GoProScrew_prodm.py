# Design inspired by https://www.thingiverse.com/thing:1643650

import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

# Params
w_grip = 20.0

h_bottom = 10.0

sphere_ratio = 1.25

d_max = 9.2

d_min = 8.7

d_outer = 14.0

d_inner = 5.5

d_grip = 11.0

h_top = 5.0

h_mid = 35.0

grip_ratio = 1.75

grip_angle = 20.0

print("Iteration", iter, vars)
# base
cube = cq.Workplane("XY").box(w_grip, w_grip, h_bottom, centered=(True, True, False))
sphere = cq.Workplane("XY").sphere(w_grip * sphere_ratio /2.0)
base = cube.intersect(sphere)
cyl_cut = cq.Workplane("XY")
for angle in range(0, 271, 90):
    temp = cq.Workplane("XY").circle(h_bottom / 2.0).extrude(h_bottom + (w_grip / 2.0)).rotate(axisStartPoint=(0,0,0), axisEndPoint=(1,0,0), angleDegrees=grip_angle).translate(vec=(0, -w_grip/grip_ratio, -w_grip/4.0)).rotate(axisStartPoint=(0,0,0),axisEndPoint=(0,0,1), angleDegrees=angle)
    cyl_cut = cyl_cut.union(temp)
base = base.cut(cyl_cut)

# Base cut
base_cut = cq.Workplane("XY").workplane(offset=-1).circle(d_grip/2.0).extrude(h_bottom+1, combine=False)

# Base hex cut from above
base_cut_hex = base.faces(">Z").workplane(offset=-1).polygon(nSides=6, diameter=d_max).workplane(offset=(h_mid+1)).polygon(nSides=6, diameter=d_min).loft(combine=False)

# Rising cyl
base = base.faces(">Z").circle(d_outer/2.0).extrude(h_mid+h_top)

# Cut inner diameter from the result
result = base.faces(">Z").hole(diameter=d_inner, depth= h_top)

# Make cuts
result = result.cut(base_cut)
result = result.cut(base_cut_hex)

# Return the block
Part.show(result.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, result)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
