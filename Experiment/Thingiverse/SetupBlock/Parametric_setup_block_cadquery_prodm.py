'''
Parametric 123 Block
Link to similar Thingiverse file: https://www.thingiverse.com/thing:3749463/files
'''

import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

moduleMajorDimension    =  25.4;
holeSizeFraction        =    .5;
ecks                    =   2;
wye                     =   1;
zed                     =   3;



number_holes_x = ecks
number_holes_y = wye
number_holes_z = zed

# Make the block
spacing = (moduleMajorDimension-(moduleMajorDimension*(holeSizeFraction/2)))


block = cq.Workplane("XY").box(spacing*number_holes_x, spacing*number_holes_y, spacing*number_holes_z)
    

# Add holes
block_x     = block.faces("<X").workplane().rarray( spacing, spacing, number_holes_y, number_holes_z, center=True).hole(moduleMajorDimension*holeSizeFraction)

block_xy    = block_x.faces("<Y").workplane().rarray( spacing, spacing, number_holes_x, number_holes_z, center=True).hole(moduleMajorDimension*holeSizeFraction)

block_xyz   = block_xy.faces("<Z").workplane().rarray( spacing, spacing, number_holes_x, number_holes_y, center=True).hole(moduleMajorDimension*holeSizeFraction)


# Return the block
Part.show(block_xyz.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, block_xyz)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, block)
listener.start_keyboard()
