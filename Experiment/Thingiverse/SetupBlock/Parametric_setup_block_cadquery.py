'''
Parametric 123 Block
Link to similar Thingiverse file: https://www.thingiverse.com/thing:3749463/files
'''

import cadquery as cq
import random
import json

# Number of iterations for the model
num_iter = 50

print("Setup Block")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()
# Number of experiment iterations

for iter in range(num_iter):
    vars = dict()
    ''' Parameters '''

    moduleMajorDimension = random.uniform(1, 100)  # Default: 25.4
    holeSizeFraction = random.uniform(0.1, 0.9)         # Default: .5
    ecks = random.randrange(1, 5)   # Default: 2
    wye = random.randrange(1, 5)    # 1;
    zed = random.randrange(1, 5)    # 3;
    vars["moduleMajorDimension"] = moduleMajorDimension
    vars["holeSizeFraction"] = holeSizeFraction
    vars["ecks"] = ecks
    vars["wye"] = wye
    vars["zed"] = zed
    
    print("Itertion", iter, vars)

    def single_block():
        """
        Construct singular blocks.
        :return: cq Solid
        """
        block = cq.Workplane("XY").box(moduleMajorDimension, moduleMajorDimension, moduleMajorDimension)
        block = block.faces("<X").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension+0.01)
        block = block.faces("<Y").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension + 0.01)
        block = block.faces("<Z").workplane().hole(diameter=moduleMajorDimension * holeSizeFraction, depth=moduleMajorDimension + 0.01)
        sphere_cut = cq.Workplane("XY").sphere(radius=moduleMajorDimension*holeSizeFraction*1.414/2.0)
        result = block.cut(sphere_cut)
        return result

    result = cq.Workplane("XY")
    for x in range(ecks):
        for y in range(wye):
            for z in range(zed):
                result_temp = single_block().translate(vec=((moduleMajorDimension*x, moduleMajorDimension*y, moduleMajorDimension*z)))
                result = result.union(result_temp)
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict, indent=2, separators=(',', ': ')))
json_file.close()

