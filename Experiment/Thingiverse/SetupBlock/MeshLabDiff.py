# Meshlab XML: https://github.com/3DLIRIOUS/MeshLabXML

import os
import re
import numpy as np
import meshlabxml as mlx

MESHLABSERVER_PATH = 'C:\\Program Files\\VCG\\MeshLab'
os.environ['PATH'] += os.pathsep + MESHLABSERVER_PATH

num_iter = 50


def make_file():
    # Create the log file
    f_log_name = "MeshlabDiff_log" + ".txt"
    f_log = open(f_log_name, "w+")
    f_log.close()
    return f_log_name


def diff_bb(f_log_name):
    """Calculate diffs and the hausdorf distance"""

    # Calculate difference for each iteration
    for iter in range(num_iter):
        cq_f_name = "cq_" + str(iter) + ".stl"
        openscad_f_name = "openscad_" + str(iter) + ".stl"

        diff = mlx.FilterScript(file_in=[cq_f_name, openscad_f_name])
        diff.parse_hausdorff = True
        mlx.sampling.hausdorff_distance(script=diff)

        diff.run_script(log=f_log_name)

        geom = mlx.FilterScript(file_in=openscad_f_name)
        mlx.compute.measure_geometry(geom)
        geom.run_script(log=f_log_name)
        print("Completed iteration: ", iter)
    return None

def clean_log(f_log_name):
    # Clean the file and save the RMS
    # Regex to extract rms data
    # I make the numpy arrays, but do not use them
    rms_data = re.compile("rms_distance *= *(.*)")
    rms_np = np.array([])
    with open(f_log_name, "r") as myfile:
        data = myfile.read()
        rms = rms_data.findall(data)
        print("Mean squared errors extracted.")
        f_summary = open("log_rms.txt", "w+")
        for iter in range(num_iter):
            f_summary.write(rms[iter] + "\n")
            rms_np = np.append(rms_np, float(rms[iter]))
        f_summary.close()
        myfile.close()

    bb_data = re.compile("aabb_diag *= *(.*)")
    bb_np = np.array([])
    with open(f_log_name, "r") as myfile:
        data = myfile.read()
        bb = bb_data.findall(data)
        print("Bounding box data extracted.")
        f_summary = open("log_bb.txt", "w+")
        for iter in range(num_iter):
            f_summary.write(bb[iter] + "\n")
            bb_np = np.append(bb_np, float(bb[iter]))
        f_summary.close()
        myfile.close()

    rms_ratio = np.array([])
    for i in range(len(rms_np)):
        rms_ratio = np.append(rms_ratio, rms_np[i] / bb_np[i])

    print("RMS ratios are ", rms_ratio)
    print("Max is ", np.max(rms_ratio))
    print("Average is ", np.mean(rms_ratio))
    f_summary = open("log_rms_ratio.txt", "w+")
    for iter in range(len(rms_ratio)):
        f_summary.write(str(rms_ratio[iter]) + "\n")
    f_summary.close()


if __name__ == '__main__':
    f_log_name = make_file()
    diff_bb(f_log_name)
    clean_log(f_log_name)