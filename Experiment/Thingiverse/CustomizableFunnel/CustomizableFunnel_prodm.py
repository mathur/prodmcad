# Inspired by the design at: thingiverse.com/thing:3243733

import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

# [Main]
# Height of bottom pipe
baseHeight = 5

# Bottom pipe diameter
baseDiam = 50

# Height of top cone
funnelHeight = 20

# Top opening
funnelTopDiam = 80

# Wall thickness
tk = 1

# [Internal]
# Non zero wall adjustments should need any change
nz = 0.5
nzm = 0.25

# Base pipe
base = cq.Workplane("XY").circle(baseDiam/2.0).extrude(baseHeight)
base_cut = cq.Workplane("XY").workplane(offset=-nzm).circle((baseDiam-tk*2.0)/2.0).extrude(baseHeight+nz, combine=False)
# Cut the base
base = base.cut(base_cut)

# Cone
cone = base.faces(">Z").circle(baseDiam/2.0).workplane(offset=funnelHeight).circle(funnelTopDiam/2.0).loft(combine=False)
cone_cut = base.faces(">Z").workplane(offset=-nzm).circle((baseDiam-tk*2.0)/2.0).workplane(offset=funnelHeight+nz).circle((funnelTopDiam-tk*2.0)/2.0).loft(combine=False)
cone = cone.cut(cone_cut)

result = base.union(cone)

Part.show(result.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, result)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
