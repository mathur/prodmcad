# Inspired by the design at: thingiverse.com/thing:3243733

import cadquery as cq
import random
import json

# Number of iterations for the model
num_iter = 50

print("CustomizableFunnel")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()

    # [Main]
    # Height of bottom pipe
    baseHeight = random.randrange(start=2, stop=100, step=1) * 1.0  # [2:1:100], Default: 5
    vars["baseHeight"] = str(baseHeight)

    # Bottom pipe diameter
    baseDiam = random.uniform(5, 200)  # [5:200], Default: 50
    vars["baseDiam"] = str(baseDiam)

    # Height of top cone
    funnelHeight = random.uniform(10, 100)  # [10:100], Default: 20
    vars["funnelHeight"] = str(funnelHeight)

    # Top opening
    funnelTopDiam = random.uniform(10, 100)  # [10:100], Default: 80
    vars["funnelTopDiam"] = str(funnelTopDiam)

    # Wall thickness
    tk = random.randrange(start=3, stop=100, step=1) * 0.1  # [0.3:0.1:10], Default: 1
    tk = min(tk, 0.49 * min(funnelTopDiam, baseDiam))  # This needs to be added for proper geometry
    vars["tk"] = str(tk)

    print("Iteration", iter)
    print(vars)
    # [Internal]
    # Non zero wall adjustments should need any change
    nz = 0.5
    nzm = 0.25

    # Base pipe
    base = cq.Workplane("XY").circle(baseDiam/2.0).extrude(baseHeight)
    base_cut = cq.Workplane("XY").workplane(offset=-nzm).circle((baseDiam-tk*2.0)/2.0).extrude(baseHeight+nz, combine=False)
    # Cut the base
    base = base.cut(base_cut)

    # Cone
    cone = base.faces(">Z").circle(baseDiam/2.0).workplane(offset=funnelHeight).circle(funnelTopDiam/2.0).loft(combine=False)
    cone_cut = base.faces(">Z").workplane(offset=-nzm).circle((baseDiam-tk*2.0)/2.0).workplane(offset=funnelHeight+nz).circle((funnelTopDiam-tk*2.0)/2.0).loft(combine=False)
    cone = cone.cut(cone_cut)

    result = base.union(cone)

    # Export to FreeCAD
    #Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()



