/*	Created by Mr Jan Bassett

	v1.0 - 2 Sep 19 - Original design
	
	Designed to have an M3 bolt running almost the length of the part as reinforcement
*/

/* [Dimensions of triangular drive:] */
// (across points measurment)
Key_AP = 9.2;
// (length)
Key_l = 8.5;
// (tip radius)
Key_tr = 0.5;

/* [Flange dimensions] */
// (diameter)
Flange_d = 15.8;
// (length)
Flange_l = 1.9;

/* [Shaft dimensions] */
// (diameter)
Shaft_d = 9.8;
// (length)
Shaft_l = 5.7;

/* [Locking pawl drive boss dimensions] */
// (width)
Drive_w = 7.1;
// (breadth)
Drive_b = 6.1;
// (length)
Drive_l = 9.1;
// (angle offset)
Drive_a = 0;
	
	{
		
	$fn=360;
		
	//Do not change
	Key_r = (0.57735 * Key_AP) - Key_tr;
	Total_l = Key_l + Flange_l + Shaft_l + Drive_l;
		
	difference() {
		union() {
			hull() {
				translate([Key_r,0,0]) cylinder(r=Key_tr,h=Key_l);
				rotate([0,0,120]) translate([Key_r,0,0]) cylinder(r=Key_tr,h=Key_l);
				rotate([0,0,240]) translate([Key_r,0,0]) cylinder(r=Key_tr,h=Key_l);
			}
			translate([0,0,Key_l]) cylinder(d=Flange_d,h=Flange_l);
			translate([0,0,Key_l+Flange_l-0.01]) cylinder(d=Shaft_d,h=Shaft_l+0.01);
			translate([0,0,Key_l+Flange_l+Shaft_l+(Drive_l/2)-0.01]) rotate([0,0,Drive_a]) cube([Drive_b,Drive_w,Drive_l+0.02],center=true);
		}
		translate([0,0,0.5]) cylinder(d=2.8,h=0.6*Total_l);
		translate([0,0,0.49+(0.6*Total_l)]) cylinder(d=3.25,h=0.4*Total_l);
	}
	
	echo ();
	echo (Bolt_Length = Total_l-0.5);
	echo ();
}