'''
Parametric Turners Cube
Link to similar Thingiverse file: https://www.thingiverse.com/thing:1072045
'''


import cadquery as cq
import random
import json

# Number of iterations for the model
num_iter = 50

print("Circular Hole Generator")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()
# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    ''' Parameters '''

    infty = 100
    L = random.uniform(1, 100)   # default 100
    vars["L"] = str(L)
    D1 = random.uniform( 3, L )   # default 90
    vars["D1"] = str(D1)
    D2 = random.uniform( 2, D1 )  # default 60;
    vars["D2"] = str(D2)
    D3 = random.uniform( 1, D2 )  # default 30;
    vars["D3"] = str(D3)

    print("Iteration", iter, vars)

    T = L/8.0;
    X1 = L/2.0-T;
    X2 = L/2.0-2.0*T;
    X3 = L/2.0-3.0*T;

    # Make the cube
    cube = cq.Workplane("XY").box(L,L,L)


    # Add smallestholes

    cube = cube.faces(">X").workplane().hole(D3, depth=X1)
    cube = cube.faces("<X").workplane().hole(D3, depth=X1)
    cube = cube.faces(">Y").workplane().hole(D3, depth=X1)
    cube = cube.faces("<Y").workplane().hole(D3, depth=X1)
    cube = cube.faces(">Z").workplane().hole(D3, depth=X1)
    cube = cube.faces("<Z").workplane().hole(D3, depth=X1)

    cube = cube.faces(">X").workplane().hole(D2, depth=X2)
    cube = cube.faces("<X").workplane().hole(D2, depth=X2)
    cube = cube.faces(">Y").workplane().hole(D2, depth=X2)
    cube = cube.faces("<Y").workplane().hole(D2, depth=X2)
    cube = cube.faces(">Z").workplane().hole(D2, depth=X2)
    cube = cube.faces("<Z").workplane().hole(D2, depth=X2)

    cube = cube.faces(">X").workplane().hole(D1, depth=X3)
    cube = cube.faces("<X").workplane().hole(D1, depth=X3)
    cube = cube.faces(">Y").workplane().hole(D1, depth=X3)
    cube = cube.faces("<Y").workplane().hole(D1, depth=X3)
    cube = cube.faces(">Z").workplane().hole(D1, depth=X3)
    result = cube.faces("<Z").workplane().hole(D1, depth=X3)

    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict, indent=2, separators=(',', ': ')))
json_file.close()



