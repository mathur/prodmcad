# This script can create any regular rectangular Lego(TM) Brick
import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
#####
# Inputs
######
lbumps = 1       # number of bumps long
wbumps = 1        # number of bumps wide
thin = True  # True for thin, False for thick

#
# Lego Brick Constants-- these make a lego brick a lego :)
#
pitch = 8.0
clearance = 0.1
bumpDiam = 4.8
bumpHeight = 1.8
if thin:
    height = 3.2
else:
    height = 9.6

t = (pitch - (2 * clearance) - bumpDiam) / 2.0
postDiam = pitch - t  # works out to 6.5
total_length = lbumps*pitch - 2.0*clearance
total_width = wbumps*pitch - 2.0*clearance

# make the base
s = cq.Workplane("XY").box(total_length, total_width, height)
tracker.add_change(shape_analyzer.get_line_num() - 1, s)

# Render the solid

listener = gui_observer.ViewListener(tracker, s, show_obj=True)
listener.start_keyboard()