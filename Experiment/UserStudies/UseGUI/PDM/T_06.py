import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Create a box
result = cq.Workplane("front").box(4.0, 4.0, 0.25)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()
