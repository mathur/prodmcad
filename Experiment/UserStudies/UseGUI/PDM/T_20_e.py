
from prodm import gui_observer
from prodm import shape_analyzer

import cadquery as cq

tracker = shape_analyzer.Tracker()
# Parameter definitions
p_outerWidth = 100.0  # Outer width of box enclosure
p_outerLength = 150.0  # Outer length of box enclosure
p_outerHeight = 50.0  # Outer height of box enclosure

p_thickness = 3.0  # Thickness of the box walls
p_sideRadius = 10.0  # Radius for the curves around the sides of the bo
p_topAndBottomRadius = 2.0  # Radius for the curves on the top and bottom edges

p_screwpostInset = 12.0  # How far in from the edges the screwposts should be
p_screwpostID = 4.0  # Inner diameter of the screwpost holes, should be roughly screw diameter not including threads
p_screwpostOD = 10.0  # Outer diameter of the screwposts. Determines overall thickness of the posts

p_boreDiameter = 8.0  # Diameter of the counterbore hole, if any
p_boreDepth = 1.0  # Depth of the counterbore hole, if
p_countersinkDiameter = 0.0  # Outer diameter of countersink. Should roughly match the outer diameter of the screw head
p_countersinkAngle = 90.0  # Countersink angle (complete angle between opposite sides, not from center to one side)
p_lipHeight = 1.0  # Height of lip on the underside of the lid. Sits inside the box body for a snug fit.

# Outer shell
oshell = cq.Workplane("XY").rect(p_outerWidth, p_outerLength) \
    .extrude(p_outerHeight + p_lipHeight)
tracker.add_change(shape_analyzer.get_line_num() - 1, oshell)
# Weird geometry happens if we make the fillets in the wrong order
if p_sideRadius > p_topAndBottomRadius:
    oshell.edges("|Z").fillet(p_sideRadius)
    tracker.add_change(shape_analyzer.get_line_num() - 1, oshell)
    oshell.edges("#Z").fillet(p_topAndBottomRadius)
    tracker.add_change(shape_analyzer.get_line_num() - 1, oshell)
# Inner shell
ishell = oshell.faces("<Z").workplane(p_thickness, True) \
    .rect((p_outerWidth - 2.0 * p_thickness), (p_outerLength - 2.0 * p_thickness)) \
    .extrude((p_outerHeight - 2.0 * p_thickness), False)
tracker.add_change(shape_analyzer.get_line_num() - 1, ishell)
ishell.edges("|Z").fillet(p_sideRadius - p_thickness)
tracker.add_change(shape_analyzer.get_line_num() - 1, ishell)

# Make the box outer box
box = oshell.cut(ishell)
tracker.add_change(shape_analyzer.get_line_num() - 1, box)

# Make the screwposts
POSTWIDTH = (p_outerWidth - 2.0 * p_screwpostInset)
POSTLENGTH = (p_outerLength - 2.0 * p_screwpostInset)

# Displays the result of this script

listener = gui_observer.ViewListener(tracker, ishell, show_obj=True)
listener.start_keyboard()
