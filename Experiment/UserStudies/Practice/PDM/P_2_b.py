import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# We make a rectangular bottle here
# Make a box of relevant length, width and height
base = cadquery.Workplane("XY").box(15, 15, 30)
tracker.add_change(shape_analyzer.get_line_num() - 1, base)
# Create a rectangular neck on top of the base
top = base.faces(">Z").workplane().box(10, 10, 20)
tracker.add_change(shape_analyzer.get_line_num() - 1, top)
# Union the base and the top
result = base.union(top)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()