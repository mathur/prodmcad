# ---------------------------------
# Trial 05
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a 3D box
result = cq.Workplane("XY").box(3, 2, 0.5)

# See Trial 05 (a) selection
result = result.faces("TODO").workplane(offset=0.75).circle(1.0).extrude(0.5)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex014