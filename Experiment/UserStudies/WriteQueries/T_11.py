# ---------------------------------
# Trial 11
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box
result = cq.Workplane("XY").box(3, 3, 0.5)

# See Trial 11 (a) selection
result = result.edges("TODO").fillet(0.125)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex020
