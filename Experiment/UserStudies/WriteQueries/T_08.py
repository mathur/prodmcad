# ---------------------------------
# Trial 08
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Make a box
result = cq.Workplane("XY").box(2, 2, 2)

# See Trial 08 (a) selection
result = result.faces("TODO").shell(-0.05)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex017