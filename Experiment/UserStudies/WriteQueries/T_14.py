# ---------------------------------
# Trial 14
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Program variables
base_l = 200.0
base_w = 200.0
base_h = 10.0
base_r = 40.0
tube_r = 25.0
tube_h = 125.0
tube_shell_r = -2.0
fillet_r = 6.0

# Make the base
base = cq.Workplane("XY").box(base_l, base_w, base_h)

# See Trial 14 (a) selection
tube = base.faces("TODO").circle(tube_r).extrude(tube_h, combine=False)

# See Trial 14 (b) selection
tube = tube.faces("TODO").shell(tube_shell_r)

result = base.union(tube)

# See Trial 14 (c) selection
result = result.edges("TODO").fillet(fillet_r)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to FilamentStorage