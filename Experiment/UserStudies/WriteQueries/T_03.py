# ---------------------------------
# Trial 03
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part 

# Make a box of given dimensions
result = cq.Workplane("XY").box(2, 3, 0.5)

# See Trial 03 (a) selection
result = result.faces("TODO").workplane().hole(0.5)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex012
