# ---------------------------------
# Trial 21
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

result = cq.Workplane("XY").box(2, 2, 2)

# See Trial 21 (a) selection
result = result.faces("TODO").shell(-0.2)

# See Trial 21 (b) selection
result = result.edges("TODO").chamfer(0.125)

Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex033