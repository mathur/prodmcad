# Inspired by https://www.thingiverse.com/thing:3767580

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer
print("TableclothClip")
thickness = 3
height = 45
length = 60
width = 15

base = cq.Workplane("XY").box(length, width, thickness)
base = base.edges(">X and <Z").circle(width/2.0).extrude(thickness)
base = base.vertices("<X and <Y and <Z").box(thickness, width, height + thickness, centered=(False,False,False))
top = base.vertices(">Z").vertices("<X and <Y").box(length, width, thickness, centered=(False,False,False), combine=False)
top = top.edges(">X and <Z").circle(width/2.0).extrude(thickness)

result = top.union(base)
result = result.edges(">Z and <X").chamfer(thickness)
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\TableclothClip.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.01)
f.close()