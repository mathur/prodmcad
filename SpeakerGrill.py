# Inspired by https://www.thingiverse.com/thing:3834348

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer
print("SpeakerGrill")
d_inner = 98
r_inner = d_inner / 2
d_outer = 126
r_outer = d_outer / 2

d_screws = 108.1
r_screws = d_screws / 2.0
d_screw = 4.0
r_screw = d_screw / 2.0
d_screwcap = 6.0
r_screwcap = d_screwcap / 2.0
screw_sink = 2.0
screw_count = 4

height = 6

fin_thickness = 2.5
fin_sink = 1.5
fin_count = 12
fin_angle = 30
fin_spacing = d_inner / (fin_count + 1)

# Two rings of half the height that are later chamfered
cyl_outer = cq.Workplane("XY").circle(r_outer).workplane(offset=height).circle((r_outer + r_inner) / 2).loft()


# Cut the inner cylinder from the outer cylinder
frame = cyl_outer.faces(">Z").hole(diameter=d_inner,depth=100)

# Add cbore holes for screws
# There is a possibility of adding a selector here
# TODO: Issue with cbore hole
hole_workplane = frame.faces(">Z").workplane()
for i in range(0, 360, 360 / screw_count):
    frame = hole_workplane.transformed(rotate=(0,0,i)).transformed(offset=(r_screws,0,0)).\
        cboreHole(cboreDepth=screw_sink, diameter=d_screw, cboreDiameter=d_screwcap)

# Fins
fins = cq.Workplane("XY")
for fin_num in range(fin_count + 1):
    fin = cq.Workplane("XY").transformed(rotate=(0, fin_angle, 0),
                                        offset=(-r_inner + fin_spacing + (fin_spacing * fin_num), 0, 0))\
        .box(fin_thickness, d_screws - d_screwcap, 100)
    fins = fins.union(fin)
# Common area of fins to be kept
common_with = cq.Workplane("XY").circle(r_screws - r_screwcap).extrude(height - fin_sink)

result = frame.union(fins.intersect(common_with))
# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\SpeakerGrill.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
f.close()