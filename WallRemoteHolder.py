# Inspired by https://www.thingiverse.com/thing:3817393

# Import statements
import cadquery as cq
import Part
import math
from prodm import gui_observer
from prodm import shape_analyzer

print("WallRemoteHolder")
#   [Remote dimensions] 

#   Standing up remote control's width
remote_width = 38
#   Standing up remote control's depth
remote_depth = 16
#   Standing up remote control's height
remote_height = 100

#   [Holder remote container height] 
#   Height of remote control holding part
remote_holder_height = 50

#   [Holder thicknesses] 
#   Holder borders thickness
box_thickness = 2
#   Holder wall side border thickness
box_back_thickness = 4

#   [Screw diameters] 
#   Screw shank diameter
screw_shank_diameter = 3
#   Screw head diameter
screw_head_diameter = 8

#   [Screw access diameter] 
#   Front hole diameter (large enough to pass the screwdriver)
front_hole_diameter = 16

#   [Screw positions] 
#   Screw top position (this model will ensure the top screw holes to be accessible)
screw_top_position = 75
#   Screw bottom position
screw_bottom_position = 25

#   [Hidden] 
fs = 0.1

# Make the outer box
box = cq.Workplane("XY").box(remote_width + 2*box_thickness, remote_depth + box_thickness + box_back_thickness, remote_height + box_thickness)
# Smooth out relevant edges
box = box.edges("|Z").fillet(box_thickness)
# rectified box
box_to_cut = cq.Workplane("XY").transformed(offset=(-box_thickness, remote_depth + box_back_thickness, -box_thickness))\
    .box(remote_width + 2 * box_thickness, box_thickness, remote_height + 2 * box_thickness, combine=False)
box = box.cut(box_to_cut)

# TODO: causes an additional subtraction at the back
remote = cq.Workplane("XY").transformed(offset=(0,0, 0)).box(remote_width, remote_depth, remote_height)
box = box.cut(remote)

# cut from above
misc = cq.Workplane("XY").transformed(offset=(0, -(remote_depth/2 + 2*box_thickness), max(min(remote_holder_height, remote_height - screw_head_diameter), front_hole_diameter + box_thickness))).box(remote_width + 2 * box_thickness, remote_depth +  2*box_thickness, remote_height + box_thickness, centered=(True,False,True))
box = box.cut(misc)

remote_half_width = remote_width/2.0

# Front hole from the bottom
bottom_hole_dist_from_bottom = max(front_hole_diameter / 2.0, min(screw_bottom_position, remote_holder_height - front_hole_diameter / 2.0))
front_screw_workplane = box.edges("<Y and <Z").workplane().transformed(offset= (0,0,bottom_hole_dist_from_bottom), rotate=(90,0,0))
box = front_screw_workplane.hole(diameter=front_hole_diameter, depth=box_back_thickness)

# csk holes
# Calculate csk angle
csk_angle = math.degrees(math.atan(box_back_thickness * 1.0 / (screw_head_diameter/2.0 - screw_shank_diameter/2.0)))
# Bottom csk hole
back_bottom_screw_workplane = front_screw_workplane.workplane(offset=-(remote_depth+box_thickness))
box = back_bottom_screw_workplane.cskHole(diameter=screw_shank_diameter, cskDiameter=screw_head_diameter, cskAngle=csk_angle, depth=box_back_thickness)
# Top csk holes
top_hole_dist_from_bottom = min(max(screw_top_position, (remote_holder_height + screw_head_diameter/2.0)), remote_height - screw_head_diameter/2)
box = back_bottom_screw_workplane.workplane().transformed(offset=(-remote_half_width/2, top_hole_dist_from_bottom - bottom_hole_dist_from_bottom,0)).cskHole(diameter=screw_shank_diameter, cskDiameter=screw_head_diameter, cskAngle=csk_angle, depth=box_back_thickness)
box = back_bottom_screw_workplane.workplane().transformed(offset=(remote_half_width/2,top_hole_dist_from_bottom - bottom_hole_dist_from_bottom,0)).cskHole(diameter=screw_shank_diameter, cskDiameter=screw_head_diameter, cskAngle=csk_angle, depth=box_back_thickness)


# Export to FreeCAD
# Part.show(box.toFreecad())
# Export STL
f = open('Exports\\WallRemoteHolder.stl', 'w')
cq.exporters.exportShape(shape=box, fileLike=f, exportType="STL",tolerance=0.01)
f.close()
