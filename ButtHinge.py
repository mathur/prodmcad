# Inspired by the model at: https://www.thingiverse.com/thing:3818734
# Modified for random sampling of parameters

import cadquery as cq
import Part
import math
import random
import json

# Number of iterations for the model
num_iter = 50

print("ButtHinge")

# What is the thickness of the leaves? (min=pin diameter+3)
leafGauge = 6

# What is the length of the pin?
pinLength = 30

# What is the width of the pin from leaf edge to leaf edge when laid flat? (min=leafGuage+20)
openWidth = 40

# What is the diameter of the pin? (min=3)
diameter = 3

# How many knuckles?
knuckleCount = 5 # [3:2:99]

# How many drill holes?
holeCount = 3 # [0:20]

truePinDiameter = max([diameter,3])
trueGauge = max([leafGauge, diameter+3])
trueWidth = max([openWidth/2.0, (trueGauge + 20)/2.0])
trueKnuckles = max([knuckleCount, 2* math.floor(knuckleCount/2)+1, 3])

knuckleRadius = trueGauge / 2.0
pinRadius = truePinDiameter / 2.0
pitch = pinLength / trueKnuckles
leafWidth = trueWidth
pClear = 0.3  # clearance between the pin and the free knuckles
kClear = 0.4  # clearance between adjacent free and fixed knuckles
support = 0.3  # support width
holeSpacingV = pinLength / (2*holeCount)
holeRadius = 1.7
chamferRadius = 3.4