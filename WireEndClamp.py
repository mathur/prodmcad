# Inspired by the model at: thingiverse.com/thing:3870946


import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

print("WireEndClamp")

"""
   Wire End Clamp for Model Railroad.
   Clamp Bus wire to end of layout, with slots to separate
   and hold two wires in position.
"""
#  PARAMETERS - values in brackets are suggested starting values)  */
# [Open Parameters:] */
# Width of clamp
b_width = 65.0  # Default: 65
# Height of clamp
b_height = 30.0  # Default: 30
# Width of gap
gap_w = 3.0  # Default: 3
# Depth of gap
gap_d = 2.0  # Default: 2
# Distance between gaps
gap_sep = 20.0  # Default: 20

# Create the main bracket
main_body = cq.Workplane("XY").box(b_width, b_height, 5, centered=(False, False, False))

# calculate and build the gaps offset from edge of bracket
offset = (b_width/2.0) -(gap_sep/2.0)-(gap_w/2.0)
offsetted_workplane = main_body.faces("<Z").transformed(offset=(offset, 0, 0)) # The base face
gap_1 = offsetted_workplane.box(gap_w, b_height, gap_d, centered=(False, False, False), combine=False)
gap_2 = offsetted_workplane.transformed(offset=(gap_sep, 0, 0)).box(gap_w, b_height, gap_d, centered=(False, False, False), combine=False)
# Union the gaps
gap = gap_1.union(gap_2)

# Countersink screw holes
offsetted_workplane = main_body.faces("<Z")
# csk hole 1
csk_1_a = offsetted_workplane.transformed(offset=(10.0,b_height/2.0,0)).circle(radius=2.0).extrude(5, combine=False)
csk_1_b = offsetted_workplane.transformed(offset=(10.0,b_height/2.0,3)).circle(radius=2.0).workplane(offset=2).circle(radius=3.675).loft(combine=False)
csk_1 = csk_1_a.union(csk_1_b)

# csk hole 2
offset = b_width - 10
csk_2_a = offsetted_workplane.transformed(offset=(offset,b_height/2.0,0)).circle(radius=2.0).extrude(5, combine=False)
csk_2_b = offsetted_workplane.transformed(offset=(offset,b_height/2.0,3)).circle(radius=2.0).workplane(offset=2).circle(radius=3.675).loft(combine=False)
csk_2 = csk_2_a.union(csk_2_b)

csk = csk_1.union(csk_2)

# Cuts
cuts = csk.union(gap)
# Cut the gaps from the main bracket
result = main_body.cut(cuts)

# Export to FreeCAD
# Part.show(result.toFreecad())
# Export STL
f = open('Exports\\WireEndClamp.stl', 'w')
cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
f.close()